# README #

Paper receipts tend to get lost, along with any right to warranty repairs. I've worked at the service department of one of the biggest electronic retail stores of the Netherlands, and encountered many people who were unable to produce a receipt when their products defect would still be covered under warranty.
But even when a products warranty is expired, consumers have rights they're  mostly unaware of.
It is my intention to improve consumer awareness concerning warranty, and their rights as consumers.
This app will help you track your purchases, receipts and warranty period. This way you won't be a victim of lost receipts, or receipts that turn black or fade.
But even after warranty expires, being able to prove the exact age will be a great asset in negotiation for a new appliance!

Dutch police advices to keep a list of serial numbers in case of theft. This way, when they find stolen items, you can prove them to be yours.

This project aims to replace boxes of paper receipts by placing them all in you pocket.
Register all your warranty covert purchases into this app, and you'll never loose your receipt again.

##Features##
* Easily register purchases with date and warranty period
* Take your own product photo so you'll definitely recognize it!
* Digitalize your receipt
* Add a friendly name like "Television living room" for convenience
* Add the serial number by scanning it's barcode
* Automatically synchronize your purchases to "The Cloud" (and back) using Google Drive

###Possible Future Features/Improvements###

* Add notification option to notify user when close to warranty expiration
* Add a way to send warranty information to another party (Receipt, make model etc). We now rely on the image viewer to do so. It's not doing a particularly great job at it though...
* Make image loading more smart about the size it loads
* Add decent input validation
* Make sure the user doesn't change the date manually
* Add quick guide and run it at first startup
* Add disclaimer (don't throw away your physical receipts!/use at your own risk)

###DONE###
* Add search option
* Add synchronization back from Google Drive
* Replace ugly icons with nice ones
* Find a way to bring dialogs back into the mix (https://code.google.com/p/android/issues/detail?id=163383)
* Add cleanup code for abandoned images (like pressing the back button on the new item dialog would leave behind)
* Add Google Drive backup for receipts and data records
* Publish this app on the Play Store!

##Credits##
* App icon, Feature graphic and UI/UX advice by Max Steenbergen http://maxsteenbergen.com 
* In app icons slightly modified from: https://www.elegantthemes.com/blog/freebie-of-the-week/beautiful-flat-icons-for-free