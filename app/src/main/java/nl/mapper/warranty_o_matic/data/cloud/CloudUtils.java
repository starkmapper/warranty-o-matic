package nl.mapper.warranty_o_matic.data.cloud;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import nl.mapper.warranty_o_matic.SettingsActivity;

/**
 * Created by stark on 08/06/15.
 */
public class CloudUtils
{
	public static void registerAuthReceiver(Context context, BroadcastReceiver receiver)
	{
		LocalBroadcastManager.getInstance(context).registerReceiver(receiver, new IntentFilter(SettingsActivity.AUTH_CLOUD_ACTION));
	}
	public static void unregisterAuthReceiver(Context context,BroadcastReceiver receiver)
	{
		LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);


	}
}
