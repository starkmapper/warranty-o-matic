package nl.mapper.warranty_o_matic.data.cloud.providers;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.drive.query.SortOrder;
import com.google.android.gms.drive.query.SortableField;

import java.io.*;
import java.lang.ref.WeakReference;
import java.util.LinkedList;

import nl.mapper.warranty_o_matic.BaseActivity;
import nl.mapper.warranty_o_matic.MainActivity;
import nl.mapper.warranty_o_matic.R;
import nl.mapper.warranty_o_matic.SettingsActivity;
import nl.mapper.warranty_o_matic.data.FileUtils;
import nl.mapper.warranty_o_matic.data.Util;
import nl.mapper.warranty_o_matic.data.cloud.StorageProvider;

/**
 * Created by stark on 5/14/15.
 */
public class GoogleDrive implements StorageProvider,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
    private static final String LOG_TAG = GoogleDrive.class.getSimpleName();

	@Override
	public LinkedList<CloudFile> listFiles(String path, String filter)
	{
		DriveFolder folder = getFolder(path);
		SortOrder order = new SortOrder.Builder()
				.addSortAscending(SortableField.TITLE)
				.build();
		MetadataBuffer buffer;
		if(filter == null)
			buffer = folder.listChildren(apiClient).await().getMetadataBuffer();
		else
		{
			Query query = new Query.Builder()
					.addFilter(Filters.contains(SearchableField.TITLE, filter))
					.build();
			buffer = folder.queryChildren(apiClient, query).await().getMetadataBuffer();
		}
		LinkedList<CloudFile> out = new LinkedList<>();
		for (Metadata meta : buffer)
			out.push(new DriveCloudFile(meta));
        if(buffer != null)
            buffer.release();
		return out;
	}

	public GoogleDrive(Context context)
    {
        this.context = context;
        init();
    }

    Context context;

	@Override
	public CloudFile openFile(String filePath)
	{
		String dir = getDirFromFilePath(filePath);
		String filename = getFilenameFromPath(filePath);
		DriveFolder folder = getFolder(dir);
		Query fileQuery = new Query.Builder()
				.addFilter(Filters.eq(SearchableField.TITLE,filename))
				.build();
		MetadataBuffer buffer = folder.queryChildren(apiClient, fileQuery).await().getMetadataBuffer();
        if (buffer != null)
        {
            CloudFile out = new DriveCloudFile(buffer.get(0));
            buffer.release();
            return out;
        }
        return null;
	}

	private void init()
    {
        apiClient = new GoogleApiClient.Builder(context)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_FILE)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private GoogleApiClient apiClient;

    @Override
    protected void finalize() throws Throwable
    {
        if (!closed)
        {
            Log.d(LOG_TAG, "Call close() when done with this object.");
        }
        super.finalize();
    }

    /**
     * Closes the connection and cleans up any data left behind
     */
    public void close()
    {
        if(apiClient.isConnected())
            apiClient.disconnect();

		closed = true;
    }

    public ConnectionResult connect()
    {
		return new DriveConnectionResult(apiClient.blockingConnect());
    }

    @Override
    public boolean refresh()
    {
        return Drive.DriveApi.requestSync(apiClient).await().isSuccess();
    }

    private boolean closed = false;

    /**
     * Alias for close.
     */
    public void disconnect()
    {
        close();
    }

    @Override
    public void onConnected(Bundle bundle)
    {
		if(connectionCallback.get() != null)
			connectionCallback.get().onConnected();
    }

    @Override
    public void onConnectionSuspended(int i)
    {

    }

	class DriveConnectionResult implements ConnectionResult
	{
		com.google.android.gms.common.ConnectionResult connectionResult;
		public DriveConnectionResult(com.google.android.gms.common.ConnectionResult result)
		{
			connectionResult = result;
		}

		@Override
		public boolean connected()
		{
			return connectionResult.isSuccess();
		}

		private void sendAuthenticateBroadcast()
		{
			Intent broadCastIntent = new Intent(SettingsActivity.AUTH_CLOUD_ACTION);
			LocalBroadcastManager bm = LocalBroadcastManager.getInstance(context);
			bm.sendBroadcast(broadCastIntent);
		}

		@Override
		public void fix(Uri uri)
		{

			if (Util.isVisible())
			{
				sendAuthenticateBroadcast();
			}
			else
			{
				// Start the settings activity to handle authentication
				Intent authActivityIntent = new Intent(context, SettingsActivity.class);
				authActivityIntent.setAction(SettingsActivity.AUTH_CLOUD_ACTION);
				if (uri != null)
					authActivityIntent.setData(uri);

				NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
						.setContentTitle(context.getString(R.string.not_sync_auth_title))
						.setContentText(context.getString(R.string.not_sync_auth_content));
				TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

				// Add main activity to the back stack, so it is just like the user entered the settings from the app
				stackBuilder.addParentStack(MainActivity.class);
				stackBuilder.addNextIntent(authActivityIntent);

				// Add the
				notificationBuilder.setContentIntent(stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT));
				// post the actual notification
				NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				if (notificationManager != null)
					notificationManager.notify(DRIVE_SYNC_NOTIFICATION_ID, notificationBuilder.build());
			}

		}

		@Override
		public boolean hasSolution()
		{
			return connectionResult.hasResolution();
		}

		private static final int DRIVE_SYNC_NOTIFICATION_ID = 891567;

        @Override
        public void fix()
        {
	        fix(null);
        }

        @Override
		public void fix(Activity activity, int requestCode)
		{
			if (connectionResult.hasResolution())
			{
				try
				{
					connectionResult.startResolutionForResult(activity,requestCode);
				}
				catch (IntentSender.SendIntentException e)
				{
					// TODO show user dialog saying we'll try again later
				}
			}
			else
			{
				GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), activity, 0).show();
			}
		}
	}

    @Override
    public void onConnectionFailed(com.google.android.gms.common.ConnectionResult connectionResult)
    {
		if (connectionCallback.get() != null)
		{
			connectionCallback.get().onConnectionFailed(new DriveConnectionResult(connectionResult));
		}
    }
	private WeakReference<ConnectionCallbacks> connectionCallback = new WeakReference<>(null);

	@Override
	public void connectAsync(ConnectionCallbacks callbacks)
	{
		connectionCallback = new WeakReference<>(callbacks);
		apiClient.connect();
	}

	@Override
    public CloudFile uploadFile(Uri file, String filePath) throws IOException
    {
        String mimeTypeString = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(file.toString()));

        InputStream is = new FileInputStream(file.getPath());
		return createFile(filePath, mimeTypeString,is);
    }


    @Override
    public CloudFile createFile(String filePath, String mimeType, InputStream contents) throws IOException
    {
        String dir = getDirFromFilePath(filePath);
        String filename = getFilenameFromPath(filePath);
        DriveFolder folder = getFolder(dir);

        MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                .setTitle(filename)
                .setMimeType(mimeType)
                .build();
        DriveContents driveContents = Drive.DriveApi.newDriveContents(apiClient).await().getDriveContents();
        OutputStream os = driveContents.getOutputStream();
        FileUtils.copy(contents, os);

        return new DriveCloudFile(folder.createFile(apiClient,changeSet, driveContents).await().getDriveFile());
    }


    private String getFilenameFromPath(String filePath)
    {
        int index = filePath.lastIndexOf(File.separator);
        if(index != -1)
        {
            return filePath.substring(index+1);
        }
        return filePath;
    }
    private String getDirFromFilePath(String filePath)
    {
        int index = filePath.lastIndexOf(File.separator);
        if(index != -1)
        {
            return filePath.substring(0, index);
        }
        return filePath;
    }

    @Override
    public boolean isConnected()
    {
        return apiClient.isConnected();
    }

    private DriveId folderID;

    private DriveFolder getFolder(String path)
    {
        if(folderID == null)
        {
            DriveFolder root = Drive.DriveApi.getRootFolder(apiClient);
            Query query = new Query.Builder()
                    .addFilter(Filters.eq(SearchableField.TITLE, path))
                    .build();
            MetadataBuffer buffer = root.queryChildren(apiClient, query).await().getMetadataBuffer();
            if (buffer != null)
            {

                for(Metadata metadata : buffer)
                {
					if (metadata.isFolder() && !metadata.isTrashed() && !metadata.isExplicitlyTrashed())
					{
						folderID = metadata.getDriveId();
						break;
					}
                }

                if(folderID == null)
                {
                    MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                            .setTitle(path)
                            .build();
                    DriveFolder.DriveFolderResult result = root.createFolder(apiClient, changeSet).await();
                    folderID = result.getDriveFolder().getDriveId();
                }
                buffer.release();
            }
        }

        return Drive.DriveApi.getFolder(apiClient, folderID);
    }

    @Override
    public boolean exists(String path)
    {
        DriveFolder folder = getFolder(getDirFromFilePath(path));
        Query query = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.TITLE, path))
                .build();
        MetadataBuffer buffer = folder.queryChildren(apiClient, query).await().getMetadataBuffer();
        boolean out = false;
        if(buffer != null)
        {
            out = buffer.getCount() > 0;
            buffer.release();
        }
        return out;
    }

    class DriveCloudFile implements CloudFile
    {
        @Override
        public void close()
        {
            if (contents != null)
            {
                contents.commit(apiClient, null).await();
                contents = null;
            }
        }

        DriveFile file;
		DriveContents contents;
		Metadata metadata;
        public DriveCloudFile(DriveFile driveFile)
        {
			file = driveFile;
        }
		public DriveCloudFile(Metadata driveMetaData)
		{
			metadata = driveMetaData.freeze();
		}

        @Override
        public boolean trash()
        {
            return file().trash(apiClient).await().isSuccess();
        }

        @Override
        public boolean isTrashed()
        {
            return metadata().isTrashed();
        }

        @Override
        public InputStream getInputStream()
        {
            InputStream out = open(DriveFile.MODE_READ_ONLY).getInputStream();
	        contents = null;
	        return out;
        }

        @Override
        public File saveToFile(Uri path) throws FileNotFoundException
        {
	        InputStream is = open(DriveFile.MODE_READ_ONLY).getInputStream();
	        if (path.getLastPathSegment().endsWith(File.separator))
		        path = path.buildUpon().appendPath(metadata().getTitle()).build();

	        File localFile = new File(path.getPath());
	        if (!localFile.exists())
	        {
		        File localDir = localFile.getParentFile();
		        if (!localDir.exists())
			        localDir.mkdirs();

		        String pathString = path.getPath();
		        FileOutputStream fileStream = new FileOutputStream(pathString);
		        try
		        {
			        FileUtils.copy(is, fileStream);
			        fileStream.close();
		        }
		        catch (IOException e)
		        {
			        Log.d(LOG_TAG, "Error saving file: ", e);
			        return null;
		        }
	        }
            return localFile;
        }

        private DriveContents open(int mode)
		{
			if(contents == null || mode != contents.getMode() )
				contents = file().open(apiClient, mode , null).await().getDriveContents();
			return contents;
		}
		private DriveFile file()
		{
			if(file == null && metadata != null)
				file = Drive.DriveApi.getFile(apiClient,metadata.getDriveId());
			return file;
		}

        @Override
        public OutputStream getOutputStream()
        {
            return open(DriveFile.MODE_WRITE_ONLY).getOutputStream();
        }

		@Override
		public boolean rename(String newName)
		{
			MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
					.setTitle(newName)
					.build();
            DriveResource.MetadataResult result = file().updateMetadata(apiClient, changeSet).await();
            metadata = result.getMetadata();
			return result.getStatus().isSuccess();
		}

		@Override
        public long getModificationTime()
        {
            return metadata().getModifiedDate().getTime();
        }

		private Metadata metadata()
		{
			if (metadata == null)
				metadata = file().getMetadata(apiClient).await().getMetadata();
			return metadata;
		}

        @Override
        public String getName()
        {
            return metadata().getTitle();
        }

        @Override
        public String getDescription()
        {
            return metadata().getDescription();
        }
    }
}

