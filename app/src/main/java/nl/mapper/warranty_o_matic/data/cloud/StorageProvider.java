package nl.mapper.warranty_o_matic.data.cloud;

import android.app.Activity;
import android.net.Uri;

import java.io.*;
import java.util.LinkedList;

/**
 * Created by stark on 5/14/15.
 */
public interface StorageProvider
{
	boolean exists(String path);

	interface CloudFile
    {

        InputStream getInputStream();
        OutputStream getOutputStream();
		long getModificationTime();
		String getName();
		boolean rename(String newName);
		String getDescription();
	    /**
	     * save this file to the specified local path.
	     * @param path Full Uri to a local path. If it is a directory (ends with "/") the cloud filename is used.
	     * @return A reference to the locally created {@link File} instance, or null on failure.
	     */
	    File saveToFile(Uri path) throws FileNotFoundException;
	    boolean trash();
	    boolean isTrashed();
		void close();
    }

    /**
     * Create file with the specified name and path
     * @param filePath path of the file to create in the cloud
     * @return reference to the created (empty) file
     */
    CloudFile createFile(String filePath, String mimeType, InputStream contents) throws IOException;

	/**
	 * Upload a locally stored file to the cloud
	 * @param file {@link Uri} to the local file
	 * @param filePath path to put the file in the cloud
	 * @return {@link CloudFile} reference allowing access to uploaded file info
	 */
	CloudFile uploadFile(Uri file, String filePath ) throws IOException;

	/**
	 * Gets a file from the cloud storage. This provides access to it's contents and metadata.
	 * @param filePath Full path to the file on the cloud storage
	 * @return {@link CloudFile} reference if successful, null otherwise
	 */
	CloudFile openFile(String filePath);
	/**
	 * Check if the cloud storage provider is connected
	 * @return true if connected
	 */
    boolean isConnected();

	/**
	 * Connect to the cloud storage provider.
	 * @return true if connecting was succesfull
	 */
	ConnectionResult connect();

	/**
	 * Refreshes the remote file cache
	 * @return true if successful
	 */
	boolean refresh();

	void connectAsync(ConnectionCallbacks callbacks);

	interface ConnectionResult
	{
		interface FixedCallback
		{
			void onFixed();
			void onError();
		}
		boolean hasSolution();
		void 	fix();

		/**
		 * Fix the connextion problem, and re-invoke sync for the specified uri
		 * @param uri uri to sync
		 */
		void 	fix(Uri uri);
		void    fix(Activity activity, int requestCode);
		boolean connected();
	}

	interface ConnectionCallbacks
	{
		void onConnectionFailed(ConnectionResult problem);
		void onConnected();
	}

	LinkedList<CloudFile> listFiles(String path, String filter);

	void disconnect();

	void close();


}
