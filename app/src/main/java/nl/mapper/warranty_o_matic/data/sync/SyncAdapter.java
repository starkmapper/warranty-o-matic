package nl.mapper.warranty_o_matic.data.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.Scanner;

import nl.mapper.warranty_o_matic.CleanupService;
import nl.mapper.warranty_o_matic.R;
import nl.mapper.warranty_o_matic.data.FileUtils;
import nl.mapper.warranty_o_matic.data.Preferences;
import nl.mapper.warranty_o_matic.data.Util;
import nl.mapper.warranty_o_matic.data.WarrantyContract;
import nl.mapper.warranty_o_matic.data.WarrantyDbHelper;
import nl.mapper.warranty_o_matic.data.WarrantyProvider;
import nl.mapper.warranty_o_matic.data.cloud.StorageProvider;
import nl.mapper.warranty_o_matic.data.cloud.providers.GoogleDrive;

/**
 * SyncAdapter to sync data to and from the cloud
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter
{
	private static final String LOG_TAG = SyncAdapter.class.getSimpleName();
	private ContentResolver resolver;
	private StorageProvider cloudStorage;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs)
	{
		super(context, autoInitialize, allowParallelSyncs);
		resolver = context.getContentResolver();
		cloudStorage = new GoogleDrive(context);
	}

	public SyncAdapter(Context context, boolean autoInitialize)
	{
		super(context, autoInitialize);
		cloudStorage = new GoogleDrive(context);
		resolver = context.getContentResolver();
	}

	private boolean connectionAllowed(Context context)
	{
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = cm.getActiveNetworkInfo();

		boolean out = false;
		// only try to connect when there is actually an active internet connection
		if (info != null && info.isConnected())
		{
			// allow sync over wifi, or if WifiOnly flag was disabled
			out |= info.getType() == ConnectivityManager.TYPE_WIFI || !Preferences.wifiOnly(context);
			// Allow sync over mobile only when not roaming
			out |= info.getType() == ConnectivityManager.TYPE_MOBILE && !info.isRoaming();
		}
		return out;
	}

	@Override
	public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult)
	{
		Log.d(LOG_TAG,"Running sync");
		Context context = getContext();
		boolean force = Preferences.forceSync(context);
		// disable forceSync after the first time.
		if(force)
			Preferences.setForceSync(context, false);
		// Allow sync when on wifi, or when WifiOnly is disabled
		if (force || connectionAllowed(context))
		{

			StorageProvider.ConnectionResult result = cloudStorage.connect();
			if (!result.connected())
			{
				result.fix();
			}
			else
			{
				cloudStorage.refresh();
				try
				{
					syncLocallyDeletedItems();
					syncDriveItems(context);
					syncNewLocalItemsToCloud();
					CleanupService.startDeleteUnusedImages(context);
				}
				catch (Exception e)
				{
					Log.e(LOG_TAG, "Error syncing to Cloud", e);
				}
				cloudStorage.close();
			}
		}
	}

	private static final String[] SYNC_TIME_PROJECTION = {WarrantyContract.WarrantyInfo.COLUMN_SYNC_TIME};
	private static final int SYNC_TIME_PROJECTION_COL_SYNC_TIME = 0;

	private void syncLocallyDeletedItems()
	{

		Cursor cursor = resolver.query(WarrantyContract.WarrantyInfo.CONTENT_URI, WARRANTY_SYNC_PROJECTION, WarrantyProvider.warrantyDeletedItemsSelection, null, null);
		for (int i = 0; cursor.moveToPosition(i); ++i)
		{
			String imageFilename    = getPathFromName(cursor.getString(COL_IMAGE));
			String receiptFilename  = getPathFromName(cursor.getString(COL_RECEIPT));
			String syncFileName     = getPathFromName(cursor.getString(COL_SYNC_ID) + ".json");
			long syncID             = cursor.getLong(COL_SYNC_ID);

			trashCloudFile(imageFilename);
			trashCloudFile(receiptFilename);
			trashCloudFile(syncFileName);
			deleteLocalRecord(WarrantyContract.WarrantyInfo.buildWarrantyUri(syncID));
		}
		cursor.close();
	}
	private void trashCloudFile(String filePath)
	{
		StorageProvider.CloudFile file = cloudStorage.openFile(filePath);
		if (file != null && !file.isTrashed())
			file.trash();
	}
	private void deleteLocalRecord(Uri uri)
	{
		resolver.delete(uri, "Any string that is not null will trigger an actual deletion", null);
	}
	private void syncDriveItems(Context context)
	{
		LinkedList<StorageProvider.CloudFile> files = cloudStorage.listFiles(context.getString(R.string.drive_folder), ".json");
		for (StorageProvider.CloudFile file : files)
		{
			try
			{
				long syncID = getSyncIDFromFileName(file.getName());
				ContentValues values = null;
				long localSyncTime = 0;
				long remoteSyncTime = file.getModificationTime();
				if (syncID < WarrantyContract.WarrantyInfo.minimalSyncID)
				{
					values = getRecordFromCloud(file);
					if (values != null)
					{
						syncID = WarrantyContract.WarrantyInfo.getSyncIdFromImageUri(Uri.parse(values.getAsString(WarrantyContract.WarrantyInfo.COlUMN_IMAGE_URI)));
						file.rename(getRecordFileName(syncID));
						// convert record data to the new format
						values = WarrantyDbHelper.updateContentValues(values);
					}
				}
				Uri recordUri = WarrantyContract.WarrantyInfo.buildWarrantyUri(syncID);
				// check local database for record
				Cursor record = resolver.query(recordUri, SYNC_TIME_PROJECTION, null, null, null);
				// save new but updated record
				if (!record.moveToFirst() && values != null)
				{
					saveCloudRecordAndImages(values, remoteSyncTime + 1);
					record.requery();
				}
				if (record.moveToFirst())
					localSyncTime = record.getLong(SYNC_TIME_PROJECTION_COL_SYNC_TIME);
				// force write of converted values to cloud

				// Delete locally if deleted on the server
				if (file.isTrashed())
				{
					//only delete if it exists locally
					if (record.moveToFirst())
						deleteLocalRecord(recordUri);
				}
				else if (remoteSyncTime > localSyncTime)
				{
					// Download record data
					values = getRecordFromCloud(file);
					if (values != null)
					{
						if (values.get(WarrantyContract.WarrantyInfo.COLUMN_SYNC_ID) == null)
							values = WarrantyDbHelper.updateContentValues(values);
						if (localSyncTime == 0)
						{
							saveCloudRecordAndImages(values, remoteSyncTime);
						}
						else
						{
							values.remove(WarrantyContract.WarrantyInfo.COLUMN_SYNC_TIME);
							values.put(WarrantyContract.WarrantyInfo.COLUMN_SYNC_TIME, remoteSyncTime);
							// update record in database
							resolver.update(recordUri, values, null, null);
						}

					}
					else
					{
						Log.d(LOG_TAG, "Failed to read cloud record");
					}

				}
				else if (localSyncTime > remoteSyncTime)
				{
					// Close syncTime cursor
					record.close();
					// Get full record
					record = resolver.query(recordUri, WARRANTY_SYNC_PROJECTION, null, null, null);
					// update record in cloud
					if (record.moveToFirst())
					{
						long syncTime = saveRecordToCloud(record, file);
						updateSyncTime(recordUri, syncTime);
					}
				}
				record.close();
			}
			catch (Exception e)
			{
				Log.e(LOG_TAG, "Error syncing item: ", e);
			}
		}
	}
	private void updateSyncTime(Uri uri, long syncTime)
	{
		ContentValues values = new ContentValues();
		values.put(WarrantyContract.WarrantyInfo.COLUMN_SYNC_TIME, syncTime);
		resolver.update(uri, values, null, null);
	}


	private void syncNewLocalItemsToCloud()
	{
		Cursor cursor = resolver.query(WarrantyContract.WarrantyInfo.CONTENT_URI, WARRANTY_SYNC_PROJECTION, WarrantyProvider.warrantyNewItemsSelection, null, null);
		saveCursorToCloud(cursor);
		cursor.close();
	}
	private void saveCursorToCloud(Cursor cursor)
	{
		for (int i = 0; cursor.moveToPosition(i); i++)
		{
			long recordSyncTime = cursor.getLong(COL_SYNC_TIME);
			if (recordSyncTime == 0)
			{

				StorageProvider.ConnectionResult result = cloudStorage.connect();
				if (result.connected())
				{
					Uri uri = WarrantyContract.WarrantyInfo.buildWarrantyUri(cursor.getLong(COL_SYNC_ID));
					Uri productImageUri = Util.imageNameToUri(getContext(), cursor.getString(COL_IMAGE));
					Uri receiptUri = Util.imageNameToUri(getContext(), cursor.getString(COL_RECEIPT));
					// TODO add retry mechanism?
					try
					{
						saveImageToCloud(receiptUri);
						saveImageToCloud(productImageUri);
						recordSyncTime = saveRecordToCloud(cursor);
						updateSyncTime(uri, recordSyncTime);
					}
					catch (IOException e)
					{
						Log.e(LOG_TAG, "Error saving record to the cloud: ", e);
					}

				} else
				{
					// TODO create notification to fix and run service again afterwards
					result.fix();
				}

			}
		}
	}


	private void saveImageToCloud(Uri imageUri) throws IOException
	{
		String filePath = getPathFromName(imageUri.getLastPathSegment());
		if (!cloudStorage.exists(filePath))
			cloudStorage.uploadFile(imageUri,filePath );
	}

	/**
	 * Saves a record to the cloud. It converts the data in the record to json and saves the JSON as a text file.
	 * @param cursor
	 * @throws IOException
	 * @return sync time
	 */
	private long saveRecordToCloud(Cursor cursor) throws IOException
	{
		// Remove syncTime from record, as the modification time on the server is used as syncTime
		String jsonString = Util.convertRecordToJson(cursor);
		String filePath = getPathFromName(getRecordFileName(cursor.getLong(COL_SYNC_ID)));
		InputStream is = new ByteArrayInputStream(jsonString.getBytes());
		if (!cloudStorage.exists(filePath))
		{
			StorageProvider.CloudFile file = cloudStorage.createFile(filePath, getContext().getString(R.string.json_mimetype), is);
			return file.getModificationTime();
		}
		return 0;
	}

	public static final String[] WARRANTY_SYNC_PROJECTION ={
		WarrantyContract.WarrantyInfo.COLUMN_FRIENDLY_NAME,
		WarrantyContract.WarrantyInfo.COlUMN_MAKE,
		WarrantyContract.WarrantyInfo.COlUMN_MODEL,
		WarrantyContract.WarrantyInfo.COlUMN_WARRANTY_DATE,
		WarrantyContract.WarrantyInfo.COlUMN_PURCHASE_DATE,
		WarrantyContract.WarrantyInfo.COlUMN_STORE,
		WarrantyContract.WarrantyInfo.COlUMN_SERIAL,
		WarrantyContract.WarrantyInfo.COlUMN_IMAGE_URI,
		WarrantyContract.WarrantyInfo.COlUMN_RECEIPT_URI,
		WarrantyContract.WarrantyInfo.COLUMN_SYNC_ID,
		WarrantyContract.WarrantyInfo.COLUMN_SYNC_TIME
};

	public static final int COL_NAME 			= 0;
	public static final int COL_MAKE 			= 1;
	public static final int COL_MODEL 			= 2;
	public static final int COL_WARRANTY_DATE 	= 3;
	public static final int COL_PURCHASE_DATE 	= 4;
	public static final int COL_PURCHASE_STORE	= 5;
	public static final int COL_PURCHASE_SERIAL	= 6;
	public static final int COL_IMAGE 			= 7;
	public static final int COL_RECEIPT 		= 8;
	public static final int COL_SYNC_ID 		= 9;
	public static final int COL_SYNC_TIME 		= 10;

	private void saveCloudRecordAndImages(ContentValues values, long syncTime)
	{

		values.remove(WarrantyContract.WarrantyInfo.COLUMN_SYNC_TIME);
		values.put(WarrantyContract.WarrantyInfo.COLUMN_SYNC_TIME, syncTime);
		String imageFileName = values.getAsString(WarrantyContract.WarrantyInfo.COlUMN_IMAGE_URI);
		String receiptFileName = values.getAsString(WarrantyContract.WarrantyInfo.COlUMN_RECEIPT_URI);

		File imageFile = null;
		try
		{
			imageFile = saveCloudFile(imageFileName);
			File receiptFile = saveCloudFile(receiptFileName);

			// save record to database when images were saved successfully
			if (imageFile != null && receiptFile != null)
			{
				values.remove(WarrantyContract.WarrantyInfo._ID);
				resolver.insert(WarrantyContract.WarrantyInfo.CONTENT_URI, values);
			}
		}
		catch (FileNotFoundException e)
		{
			Log.e(LOG_TAG, "Failed to save cloud images to disk: ", e);
		}
	}
	private long saveRecordToCloud(Cursor values, StorageProvider.CloudFile file) throws IOException
	{
		String jsonString = Util.convertRecordToJson(values);
		InputStream is = new ByteArrayInputStream(jsonString.getBytes());
		OutputStream os = file.getOutputStream();
		FileUtils.copy(is, os);
		file.close();
		return file.getModificationTime();
	}
	private ContentValues getRecordFromCloud(long syncID)
	{
		String filename = getRecordFileName(syncID);
		StorageProvider.CloudFile file = cloudStorage.openFile(getPathFromName(filename));
		return getRecordFromCloud(file);
	}

	private File saveCloudFile(String fileName) throws FileNotFoundException
	{
		if (Util.storageAvailable())
		{
			StorageProvider.CloudFile file = cloudStorage.openFile(fileName);
			Uri storageUri = Util.imageNameToUri(getContext(), fileName);
			return file.saveToFile(storageUri);
		}
		return null;
	}
	long getSyncIDFromFileName(String fileName)
	{
		return Long.parseLong(fileName.substring(0, fileName.lastIndexOf(".json")));
	}
	private ContentValues getRecordFromCloud(StorageProvider.CloudFile file)
	{
		InputStream is = file.getInputStream();
		Scanner s = new Scanner(is);
		if (s.hasNext())
			try
			{
				return Util.convertJsonToRecord(s.useDelimiter("\\A").next());
			}
			catch (JSONException e)
			{
				Log.d(LOG_TAG,"Failed to convert cloud record from JSON: ", e);
			}

		return null;
	}

	private String getRecordFileName(long syncID)
	{
		return Long.toString(syncID) + ".json";
	}

	String getPathFromName(String name)
	{
		return getContext().getString(R.string.drive_folder) + File.separator + name;
	}

	public static void initializeSync(Context context)
	{
		getAccount(context);
	}

	public static void enable(Context context)
	{
		setupAccount(context,getAccount(context));
	}

	public static void disable(Context context)
	{
		disableSync(context, getAccount(context));
	}

	public static Account getAccount(Context context)
	{
		AccountManager accountManager = (AccountManager)context.getSystemService(Context.ACCOUNT_SERVICE);
		String accountType = context.getString(R.string.sync_account_type);
		Account out = new Account(context.getString(R.string.app_name), accountType);
		if (!accountExists(accountManager, out, accountType))
			if(accountManager.addAccountExplicitly(out,"",null))
			{
				//setup sync account when newly created
				setupAccount(context, out);
				return out;
			}

		return out;
	}

	private static boolean accountExists(AccountManager manager, Account account, String type)
	{
		return manager.getPassword(account) != null;
	}

	public static void configureSyncInterval(Context context, int syncInterval, int flexTime)
	{
		Account account = getAccount(context);
		configureSyncInterval(context,account,syncInterval,flexTime);

	}
	public static void configureSyncInterval(Context context, Account account, int syncInterval, int flexTime)
	{
		String authority = context.getString(R.string.content_authority);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			// we can enable inexact timers in our periodic sync
			SyncRequest request = new SyncRequest.Builder().
					syncPeriodic(syncInterval, flexTime).
					setSyncAdapter(account, authority).
					setExtras(new Bundle()).build();
			ContentResolver.requestSync(request);
		} else {
			ContentResolver.addPeriodicSync(account, authority, new Bundle(), syncInterval);
		}
	}

	public static void enableSync(Context context, Account account)
	{
		ContentResolver.setSyncAutomatically(account,context.getString(R.string.content_authority),true);
	}
	public static void disableSync(Context context, Account account)
	{
		ContentResolver.setSyncAutomatically(account,context.getString(R.string.content_authority),false);
	}

	public static void setupAccount(Context context, Account account)
	{
		int syncInterval = Preferences.getSyncInterval(context);
		int syncFlex     = Preferences.getSyncFlex(context);
		configureSyncInterval(context,account,syncInterval,syncFlex);
		enableSync(context,account);
	}

	public static void syncNow(Context context)
	{
		Bundle syncBundle = new Bundle();
		syncBundle.putBoolean(
				ContentResolver.SYNC_EXTRAS_MANUAL, true);
		syncBundle.putBoolean(
				ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

		ContentResolver.requestSync(getAccount(context),context.getString(R.string.content_authority),syncBundle);
	}
}
