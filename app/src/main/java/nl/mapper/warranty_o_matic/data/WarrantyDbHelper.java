package nl.mapper.warranty_o_matic.data;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

import nl.mapper.warranty_o_matic.data.WarrantyContract.WarrantyInfo;
/**
 * Manages connections to local database
 */
public class WarrantyDbHelper extends SQLiteOpenHelper
{
	private static final String LOG_TAG = WarrantyDbHelper.class.getSimpleName();
	public static final int DATABASE_VERSION = 2;
	private final int REQUESTED_DATABASE_VERSION;

	private static final String DATABASE_NAME = "warranty.db";
	@TargetApi(11)
	public WarrantyDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler)
	{
		super(context, name, factory, version, errorHandler);
		REQUESTED_DATABASE_VERSION = version;
	}

	public WarrantyDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
	{
		super(context, name, factory, version);
		REQUESTED_DATABASE_VERSION = version;
	}

	public WarrantyDbHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		REQUESTED_DATABASE_VERSION = DATABASE_VERSION;
	}

	public static final String UPGRADE_QUERY_FROM_VERSION_1[] = new String[]
			{
					"ALTER TABLE " + WarrantyInfo.TABLE_NAME
							+ " ADD COLUMN " + WarrantyInfo.COLUMN_SYNC_ID+ " INTEGER;"
					, "ALTER TABLE " + WarrantyInfo.TABLE_NAME
					+ " ADD COLUMN " + WarrantyInfo.COLUMN_SYNC_TIME + " INTEGER;"
			};

	final String SQL_CREATE_WARRANTY_TABLE[] = new String[]
			{
					"CREATE TABLE " + WarrantyInfo.TABLE_NAME
					+ " (" +
					WarrantyInfo._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
					WarrantyInfo.COlUMN_PURCHASE_DATE + " INTEGER NOT NULL," +
					WarrantyInfo.COlUMN_WARRANTY_DATE + " INTEGER NOT NULL, " +
					WarrantyInfo.COLUMN_FRIENDLY_NAME + " TEXT NOT NULL, " +
					WarrantyInfo.COlUMN_MAKE + " TEXT NOT NULL, " +
					WarrantyInfo.COlUMN_MODEL+ " TEXT NOT NULL, " +
					WarrantyInfo.COlUMN_STORE + " TEXT NOT NULL, " +
					WarrantyInfo.COlUMN_SERIAL + " TEXT NOT NULL, " +
					WarrantyInfo.COlUMN_IMAGE_URI + " TEXT NOT NULL, " +
					WarrantyInfo.COlUMN_RECEIPT_URI + " TEXT NOT NULL " +
					");"
					,"CREATE TABLE " + WarrantyInfo.TABLE_NAME
							+ " (" +
							WarrantyInfo._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
							WarrantyInfo.COlUMN_PURCHASE_DATE + " INTEGER NOT NULL," +
							WarrantyInfo.COlUMN_WARRANTY_DATE + " INTEGER NOT NULL, " +
							WarrantyInfo.COLUMN_FRIENDLY_NAME + " TEXT NOT NULL, " +
							WarrantyInfo.COlUMN_MAKE + " TEXT NOT NULL, " +
							WarrantyInfo.COlUMN_MODEL+ " TEXT NOT NULL, " +
							WarrantyInfo.COlUMN_STORE + " TEXT NOT NULL, " +
							WarrantyInfo.COlUMN_SERIAL + " TEXT NOT NULL, " +
							WarrantyInfo.COlUMN_IMAGE_URI + " TEXT NOT NULL, " +
							WarrantyInfo.COlUMN_RECEIPT_URI + " TEXT NOT NULL, " +
							WarrantyInfo.COLUMN_SYNC_ID+ " INTEGER UNIQUE NOT NULL, " +
							WarrantyInfo.COLUMN_SYNC_TIME + " INTEGER " +
							");"
			};

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		switch(oldVersion)
		{
			case 1:
				upgradeVersion1To2(db, Util.getSyncTime());
		}

	}
	private static final String SELECT_ALL_QUERY = "SELECT * FROM " + WarrantyInfo.TABLE_NAME +";";
	public static void upgradeVersion1To2(SQLiteDatabase db, long syncTime)
	{
		for(String query : UPGRADE_QUERY_FROM_VERSION_1)
			db.execSQL(query);

		Cursor cursor = db.rawQuery(SELECT_ALL_QUERY, null);
		int count 		= cursor.getCount();
		int position 	= 0;
		while (cursor.moveToPosition(position++))
			updateSyncIdFromPictureUri(db, cursor, syncTime);

	}
	private static final String UPDATE_SELECTION_SYNC_ID 	= WarrantyContract.WarrantyInfo._ID + " = ?";
	private static void updateSyncIdFromPictureUri(SQLiteDatabase db, Cursor cursor, long syncTime)
	{
		try
		{
			ContentValues newValues = convertRecordVersion1To2(cursor, syncTime);
			String rowID = cursor.getString(cursor.getColumnIndex(WarrantyInfo._ID));
			int result = db.update(WarrantyInfo.TABLE_NAME,newValues,UPDATE_SELECTION_SYNC_ID,new String[]{rowID});
		}
		catch (Exception e)
		{
			Log.e(LOG_TAG, "Error updating Sync ID", e);
		}

	}


	private static ContentValues convertRecordVersion1To2(Cursor cursor, long syncTime)
	{
		String imageUriString = cursor.getString(cursor.getColumnIndex(WarrantyInfo.COlUMN_IMAGE_URI));
		String receiptUriString = cursor.getString(cursor.getColumnIndex(WarrantyInfo.COlUMN_RECEIPT_URI));
		Uri imageUri = Uri.parse(imageUriString);
		Uri receiptUri = Uri.parse(receiptUriString);
		long syncID = WarrantyInfo.getSyncIdFromImageUri(imageUri);

		return getUpdateContentValues(syncID, syncTime, imageUri, receiptUri);
	}

	public static ContentValues convertCursorValues(Cursor cursor)
	{
		ContentValues values = new ContentValues();
		transferLong(values, cursor, WarrantyInfo.COlUMN_PURCHASE_DATE);

		transferLong(values,cursor,WarrantyInfo.COlUMN_WARRANTY_DATE );
		transferString(values,cursor,WarrantyInfo.COLUMN_FRIENDLY_NAME);
		transferString(values,cursor,WarrantyInfo.COlUMN_MAKE );
		transferString(values,cursor,WarrantyInfo.COlUMN_MODEL);
		transferString(values,cursor,WarrantyInfo.COlUMN_STORE );
		transferString(values,cursor,WarrantyInfo.COlUMN_SERIAL);
		transferString(values,cursor,WarrantyInfo.COlUMN_IMAGE_URI);
		transferString(values,cursor,WarrantyInfo.COlUMN_RECEIPT_URI );
		transferLong(values,cursor,WarrantyInfo.COLUMN_SYNC_ID);
		transferLong(values,cursor,WarrantyInfo.COLUMN_SYNC_TIME);
		return values;
	}
	private static void transferLong(ContentValues target, Cursor source, String column)
	{
		target.put(column, source.getLong(source.getColumnIndex(column)));
	}
	private static void transferString(ContentValues target, Cursor source, String column)
	{
		target.put(column, source.getString(source.getColumnIndex(column)));
	}


	public static ContentValues getUpdateContentValues(long syncID, long syncTime, Uri imageUri, Uri receiptUri)
	{
		ContentValues newValues = new ContentValues();
		newValues.put(WarrantyInfo.COLUMN_SYNC_ID, syncID);
		newValues.put(WarrantyInfo.COLUMN_SYNC_TIME, syncTime);
		// Convert full image uri's to filenames
		newValues.put(WarrantyInfo.COlUMN_IMAGE_URI, imageUri.getLastPathSegment());
		newValues.put(WarrantyInfo.COlUMN_RECEIPT_URI, receiptUri.getLastPathSegment());
		return newValues;
	}
	public static ContentValues updateContentValues(ContentValues oldValues)
	{
		ContentValues newValues = new ContentValues(oldValues);
		long syncTime   = 0;
		Uri imageUri    = Uri.parse(newValues.getAsString(WarrantyInfo.COlUMN_IMAGE_URI));
		Uri receiptUri  = Uri.parse(newValues.getAsString(WarrantyInfo.COlUMN_RECEIPT_URI));
		long syncID     = WarrantyInfo.getSyncIdFromImageUri(imageUri);

		newValues.remove(WarrantyInfo.COLUMN_SYNC_ID);
		newValues.remove(WarrantyInfo.COLUMN_SYNC_TIME);
		newValues.remove(WarrantyInfo.COlUMN_IMAGE_URI);
		newValues.remove(WarrantyInfo.COlUMN_RECEIPT_URI);

		newValues.putAll(getUpdateContentValues(syncID, syncTime, imageUri, receiptUri));

		return newValues;
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		createDatabase(db, REQUESTED_DATABASE_VERSION);
	}

	private void createDatabase(SQLiteDatabase db, int version)
	{
		db.execSQL(SQL_CREATE_WARRANTY_TABLE[version-1]);
		Log.d(LOG_TAG, SQL_CREATE_WARRANTY_TABLE[version-1]);
	}
}
