package nl.mapper.warranty_o_matic.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Helper utils for files and streams
 */
public class FileUtils
{
	private static final int DEFAULT_BUFFER_SIZE = 1024;
	public static int copy(InputStream source, OutputStream destination) throws IOException
	{
		return copy(source,destination,DEFAULT_BUFFER_SIZE);
	}

	public static int copy(InputStream source, OutputStream destination, int bufferSize) throws IOException
	{
		int bytesWritten = 0;
		int bytesRead;
		byte buffer[] = new byte[bufferSize];
		while ((bytesRead = source.read(buffer)) != -1)
			destination.write(buffer,0,bytesRead);

		return bytesWritten;
	}
}
