package nl.mapper.warranty_o_matic.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import nl.mapper.warranty_o_matic.R;

/**
 * Created by mwstappe on 17-4-2015.
 */
public class Preferences
{
	public static boolean driveConfigured(Context context)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getBoolean(context.getString(R.string.pref_drive_configured),false);
	}
	public static boolean syncToDrive(Context context)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getBoolean(context.getString(R.string.pref_sync_to_drive), false);
	}
	public static void setSyncToDrive(Context context, boolean state)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor edit = prefs.edit();
		edit.putBoolean(context.getString(R.string.pref_sync_to_drive), state);
		edit.apply();
		setSyncToDriveConfigured(context);
	}
	public static void setSyncToDriveConfigured(Context context)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor edit = prefs.edit();
		edit.putBoolean(context.getString(R.string.pref_drive_configured), true);
		edit.apply();
	}
	public static void setLastSyncTime(Context context,long syncTime)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor edit = prefs.edit();
		edit.putLong(context.getString(R.string.pref_sync_time), syncTime);
		edit.apply();

	}


	public static long getLastSyncTime(Context context, long syncTime)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getLong(context.getString(R.string.pref_sync_time), 0);
	}

	private static final int SYNC_INTERVAL = 3600 * 24;
	private static final int SYNC_FLEX = 3600 * 6;

	public static int getSyncInterval(Context context)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getInt(context.getString(R.string.sync_interval), SYNC_INTERVAL);
	}
	public static int getSyncFlex(Context context)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getInt("sync_flex", SYNC_FLEX);
	}

	public static boolean wifiOnly(Context context)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getBoolean(context.getString(R.string.pref_wifi_only),true);
	}

	public static boolean forceSync(Context context)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getBoolean(context.getString(R.string.pref_force_sync), false);
	}
	public static void setForceSync(Context context, boolean value)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor edit = prefs.edit();
		edit.putBoolean(context.getString(R.string.pref_force_sync), value);
		edit.apply();
	}


}
