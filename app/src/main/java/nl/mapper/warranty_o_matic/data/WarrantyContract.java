package nl.mapper.warranty_o_matic.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.format.Time;

import nl.mapper.warranty_o_matic.R;


/**
 * Contract for warranty database
 */
public class WarrantyContract
{
	public final static String CONTENT_AUTHORITY = "nl.mapper.warranty-o-matic";

	public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

	public static final String WARRANTY_PATH = "warranty";

	public static final class WarrantyInfo implements BaseColumns
	{
		public static final String TABLE_NAME = "warranty";

		public static final String COLUMN_FRIENDLY_NAME 	= "name";
		/** Purchase date in Unix time */
		public static final String COlUMN_PURCHASE_DATE 	= "date";
		public static final String COlUMN_WARRANTY_DATE		= "warranty";
		public static final String COlUMN_STORE				= "store";
		public static final String COlUMN_SERIAL			= "serial";
		public static final String COlUMN_MAKE				= "make";
		public static final String COlUMN_MODEL				= "model";

		public static final String COlUMN_IMAGE_URI 		= "image";
		public static final String COlUMN_RECEIPT_URI		= "receipt";

        /** id to uniquely identify this item */
		public static final String COLUMN_SYNC_ID 			= "syncID";
        /** Time this item was last synched/changed in ms since epoch */
		public static final String COLUMN_SYNC_TIME 		= "syncTime";


		public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(WARRANTY_PATH).build();
		public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE +"/"+CONTENT_AUTHORITY + "/" + WARRANTY_PATH;
		public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE +"/"+CONTENT_AUTHORITY + "/" + WARRANTY_PATH;

		public static final String TEXT_SEARCH_QUERY = "q";

		/** Use a minimal sync time to be able to distinct between old and new file formats */
		public static long minimalSyncID = 1421625600000L;

		public static Uri buildWarrantyUri(long id)
		{return ContentUris.withAppendedId(CONTENT_URI,id);}

		public static Uri buildWarrantyExpiresWithinDays(int days)
		{
			Time time = new Time(Time.TIMEZONE_UTC);
			int julianDay = Time.getJulianDay(System.currentTimeMillis(),0) + days;
			long expireDate =  time.setJulianDay(julianDay);
			return CONTENT_URI.buildUpon().appendQueryParameter(COlUMN_WARRANTY_DATE, Long.toString(expireDate)).build();
		}

		public static Uri buildUriWithTextSearch(String text)
		{
			return CONTENT_URI.buildUpon().appendQueryParameter(TEXT_SEARCH_QUERY, text).build();
		}

		public static String getTextSearchQueryFromUri(Uri uri)
		{
			return uri.getQueryParameter(TEXT_SEARCH_QUERY);
		}

		public static String getWarrantyExpiresDateFromUri(Uri uri)
		{
			return uri.getQueryParameter(COlUMN_WARRANTY_DATE);
		}
		public static String getWarrantyIDStringFromUri(Uri uri)
		{
			return uri.getPathSegments().get(1);
		}
		public static long getSyncIdFromImageUri(Uri uri)
		{
			long syncTime = 0;
			// Filename is in the format IMG_YYYYMMDD_HHMMSS
			String fileName = uri.getLastPathSegment();
			if(fileName.contains("IMG_"))
			{
				String dateString = fileName.substring(4, 12);
				dateString += fileName.substring(13, 19);
				syncTime = Util.convertDateStringToMillis(dateString);
			}
			else
			{
				syncTime = Long.parseLong(fileName.substring(0,fileName.indexOf(".")));
			}
			if (syncTime == 0)
				syncTime = System.currentTimeMillis();

			return syncTime;
		}

	}

}
