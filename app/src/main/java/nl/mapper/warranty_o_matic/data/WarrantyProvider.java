package nl.mapper.warranty_o_matic.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import nl.mapper.warranty_o_matic.data.WarrantyContract.WarrantyInfo;

/**
 * Data class for warranty info
 */
public class WarrantyProvider extends ContentProvider
{
	private WarrantyDbHelper dbHelper;


	private static final UriMatcher matcher = buildUriMatcher();
	static final int WARRANTY_LIST  		= 12;
	static final int WARRANTY_ITEM			= 14;

	static UriMatcher buildUriMatcher()
	{
		UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

		// List path (base)
		matcher.addURI( WarrantyContract.CONTENT_AUTHORITY, WarrantyContract.WARRANTY_PATH, WARRANTY_LIST);
		// ID path
		matcher.addURI(WarrantyContract.CONTENT_AUTHORITY, WarrantyContract.WARRANTY_PATH + "/#", WARRANTY_ITEM);

		return matcher;
	}
	public static final String warrantyExpiresSelection = WarrantyInfo.COlUMN_WARRANTY_DATE + " <= ?";
	public static final String warrantyItemSelection = WarrantyInfo._ID + " = ? AND " + WarrantyInfo.COLUMN_SYNC_TIME + " IS NOT -1";
	public static final String warrantyDeletedItemsSelection = WarrantyInfo.COLUMN_SYNC_TIME + " = -1";
	public static final String warrantyItemsSelection = WarrantyInfo.COLUMN_SYNC_TIME + " IS NOT -1";
	public static final String warrantySyncItemSelection = WarrantyInfo.COLUMN_SYNC_ID + " = ? AND " + WarrantyInfo.COLUMN_SYNC_TIME + " IS NOT -1";
	public static final String warrantySyncItemDeleteSelection = WarrantyInfo.COLUMN_SYNC_ID + " = ?";
	public static final String warrantyItemDeleteSelection = WarrantyInfo._ID + " = ?";

	public static final String warrantyNewItemsSelection = WarrantyInfo.COLUMN_SYNC_TIME + " = 0 OR " + WarrantyInfo.COLUMN_SYNC_TIME + " IS NULL" ;
	public static final String warrantyQuerySelection = WarrantyInfo.COLUMN_FRIENDLY_NAME + " LIKE ? OR "+ WarrantyInfo.COlUMN_MAKE + " LIKE ? OR "+ WarrantyInfo.COlUMN_MODEL + " LIKE ? OR "+ WarrantyInfo.COlUMN_SERIAL + " LIKE ? OR "+ WarrantyInfo.COlUMN_STORE + " LIKE ?";

	public static final SQLiteQueryBuilder queryBuilder;
	static
	{
		queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(WarrantyInfo.TABLE_NAME);
	}
	@Override
	public boolean onCreate()
	{
		dbHelper = new WarrantyDbHelper(getContext());
		return true;
	}
	Cursor getWarrantyExpiresList(Uri uri, String[] projection, String sortingOrder)
	{
		String expireDate = WarrantyInfo.getWarrantyExpiresDateFromUri(uri);
		String query      = WarrantyInfo.getTextSearchQueryFromUri(uri);
		String selection = null;
		String[] selectionArgs = null;
		if(expireDate != null)
		{
			selection = warrantyExpiresSelection;
			selectionArgs = new String[]{expireDate};
		}
		else if(query != null)
		{
			selection = warrantyQuerySelection;
			selectionArgs = new String[]{
					"%" + query + "%",
					"%" + query + "%",
					"%" + query + "%",
					"%" + query + "%",
					"%" + query + "%"};
		}
		if (selection == null)
		{
			selection = warrantyItemsSelection;

		}
		return queryBuilder.query(dbHelper.getReadableDatabase(),projection,selection,selectionArgs,null,null,sortingOrder);
	}

	Cursor getWarrantyItem(Uri uri, String[] projection)
	{
		String warrantyID = WarrantyInfo.getWarrantyIDStringFromUri(uri);

		return queryBuilder.query(dbHelper.getReadableDatabase(), projection, getWarrantyItemSelection(warrantyID), new String[]{warrantyID}, null, null, null);
	}

	private String getWarrantyItemSelection(String syncID)
	{
		return getWarrantyItemSelection(Long.parseLong(syncID));
	}
	private String getWarrantyItemSelection(long syncID)
	{
		if(syncID > WarrantyInfo.minimalSyncID)
			return warrantySyncItemSelection;
		return warrantyItemSelection;
	}
	private String getWarrantyItemDeleteSelection(String syncID)
	{
		return getWarrantyItemDeleteSelection(Long.parseLong(syncID));
	}

	private String getWarrantyItemDeleteSelection(long syncID)
	{
		if(syncID > WarrantyInfo.minimalSyncID)
			return warrantySyncItemDeleteSelection;
		return warrantyItemDeleteSelection;
	}
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
	{
		Cursor out;
		switch (matcher.match(uri))
		{
			case WARRANTY_LIST:
				if(selection == null)
					out = getWarrantyExpiresList(uri,projection,sortOrder);
				else
					out = queryBuilder.query(dbHelper.getReadableDatabase(),projection,selection,selectionArgs, null, null, sortOrder);

				break;
			case WARRANTY_ITEM:
				out = getWarrantyItem(uri, projection);
				break;
			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
		if(out != null)
			out.setNotificationUri(getContext().getContentResolver(),uri);

		return out;
	}

	@Override
	public String getType(Uri uri)
	{
		switch (matcher.match(uri))
		{
			case WARRANTY_LIST:
				return WarrantyInfo.CONTENT_TYPE;
			case WARRANTY_ITEM:
				return WarrantyInfo.CONTENT_ITEM_TYPE;
			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
	}

	/**
	 * Insert passed data and notify listeneres of changes
	 * @param uri Uri defining the value type
	 * @param values values to insert into the data backend
	 * @return Uri of the newly inserted data
	 */
	@Override
	public Uri insert(Uri uri, ContentValues values)
	{
		final SQLiteDatabase db = dbHelper.getWritableDatabase();
		Uri outUri = null;
		int match = matcher.match(uri);
		switch (match)
		{
			case WARRANTY_LIST:
				long rowID = db.insert(WarrantyInfo.TABLE_NAME, null,values);
				if(rowID > 0)
					outUri = WarrantyInfo.buildWarrantyUri(rowID);
				else
					throw new SQLException("Failed to insert row: "+uri);
				break;
			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}

		if (outUri != null)
			notify(uri, getContext());

		return outUri;
	}

	/**
	 * Delete records from the database, and notify of change if anything is actually deleted.
	 * @param uri Uri of data do delete
	 * @param selection selection string. if set to "null" everything is deleted.
	 * @param selectionArgs Arguments for selection string
	 * @return number of deleted rows
	 */
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs)
	{
		String tableName = WarrantyInfo.TABLE_NAME;
		switch (matcher.match(uri))
		{
			case WARRANTY_LIST:

				break;
			case WARRANTY_ITEM:
				// set synctime to -1 to mark record for deletion
				if (selection == null)
				{
					ContentValues values = new ContentValues();
					values.put(WarrantyInfo.COLUMN_SYNC_TIME, -1);
					return update(uri, values,null,null);
				}
				else
				{
					String warrantyID = WarrantyInfo.getWarrantyIDStringFromUri(uri);
					selection = getWarrantyItemDeleteSelection(warrantyID);
					selectionArgs = new String[]{warrantyID};
				}
				break;

			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
		// delete all on empty selection
		if(selection == null)
			selection = "1";

		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int rowsDeleted = db.delete(tableName,selection,selectionArgs);
		if(rowsDeleted > 0)
			notify(uri,getContext());

		return rowsDeleted;
	}

	public static void notify(Uri uri, Context context)
	{
		context.getContentResolver().notifyChange(uri,null);
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
	{
		String tableName = WarrantyInfo.TABLE_NAME;

		switch (matcher.match(uri))
		{
			case WARRANTY_LIST:
				break;
			case WARRANTY_ITEM:
				String warrantyID = WarrantyInfo.getWarrantyIDStringFromUri(uri);
				selection = getWarrantyItemDeleteSelection(warrantyID);
				selectionArgs = new String[]{warrantyID};
				break;
			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int rowsAffected= db.update(tableName,values,selection,selectionArgs);
		if(rowsAffected>0)
			notify(uri, getContext());
		return rowsAffected;

	}

	@Override
	public void shutdown()
	{
		dbHelper.close();
		super.shutdown();
	}
}
