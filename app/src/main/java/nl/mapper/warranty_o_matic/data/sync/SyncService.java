package nl.mapper.warranty_o_matic.data.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Service run for syncing
 */
public class SyncService extends Service
{
	private static SyncAdapter syncAdapter = null;
	private static final Object syncObject = new Object();

	@Override
	public void onCreate()
	{
		synchronized (syncObject)
		{
			if(syncAdapter == null)
				syncAdapter = new SyncAdapter(getApplicationContext(),true);
		}
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent)
	{
		return syncAdapter.getSyncAdapterBinder();
	}
}
