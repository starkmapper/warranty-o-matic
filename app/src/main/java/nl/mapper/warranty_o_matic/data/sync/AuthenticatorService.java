package nl.mapper.warranty_o_matic.data.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by stark on 02/06/15.
 */
public class AuthenticatorService extends Service
{
	private Authenticator authenticator;

	@Override
	public void onCreate()
	{
		authenticator = new Authenticator(this);
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent)
	{
		return authenticator.getIBinder();
	}
}
