package nl.mapper.warranty_o_matic.data;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.*;

import nl.mapper.warranty_o_matic.BaseActivity;

/**
 * Created by stark on 5/5/15.
 */
public class Util
{
    private static final String LOG_TAG = Util.class.getSimpleName();
    public static String getSyncTimeString()
    {
        long syncTime = getSyncTime();
        return Long.toString(syncTime);
    }
    public static long getSyncTime()
    {
        return System.currentTimeMillis();
    }

    public static long convertDateStringToMillis(String dateString)
    {
        try
        {
            int year    = Integer.parseInt(dateString.substring(0,4));
            int month   = Integer.parseInt(dateString.substring(4,6));
            int day     = Integer.parseInt(dateString.substring(6,8));
            int hour    = Integer.parseInt(dateString.substring(8,10));
            int minute    = Integer.parseInt(dateString.substring(10,12));
            int second    = Integer.parseInt(dateString.substring(12));
            calculationCalendar.setTimeInMillis(0);
            calculationCalendar.set(year,month-1,day,hour,minute,second);
            return calculationCalendar.getTimeInMillis();
        }
        catch(Exception e)
        {
            Log.e(LOG_TAG, "Error converting date", e);
        }
        return 0;
    }

    public static String convertRecordToJson(Cursor cursor)
    {
        JSONObject json = new JSONObject();
        int count = cursor.getColumnCount();
        for (int i =0; i < count; i++)
        {
            try
            {
	            String columnName = cursor.getColumnName(i);
	            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
	            {
		            switch (cursor.getType(i))
		            {
			            case Cursor.FIELD_TYPE_FLOAT:
				            json.put(columnName, cursor.getDouble(i));
				            break;
			            case Cursor.FIELD_TYPE_INTEGER:
				            json.put(columnName, cursor.getLong(i));
				            break;
			            case Cursor.FIELD_TYPE_STRING:
				            json.put(columnName, cursor.getString(i));
				            break;
		            }
	            }
	            else
	            {
		            json.put(columnName, cursor.getString(i));
	            }
            }
            catch (JSONException e)
            {
                Log.e(LOG_TAG,"Error parsing record: ",e);
            }
        }
        return json.toString();
    }

    public static String convertRecordToJson(ContentValues values)
    {
        JSONObject json = new JSONObject();
	    Set<Map.Entry<String,Object>> valueSet = values.valueSet();
	    for (Map.Entry<String, Object> entry : valueSet)
	    {
		    try
		    {
				json.put(entry.getKey(), entry.getValue());
		    }
		    catch (JSONException e)
		    {
			    Log.e(LOG_TAG,"Error parsing record: ",e);
		    }
	    }
        return json.toString();
    }
	public static ContentValues convertJsonToRecord(String jsonString) throws JSONException
	{
		JSONObject json = new JSONObject(jsonString);
		Iterator<String> keys = json.keys();
		ContentValues values = new ContentValues();
		while (keys.hasNext())
		{
			String key = keys.next();
			Object o = json.get(key);
			if( o != null )
				if(o.getClass() == Long.class)
					values.put(key,(Long)o);
				else if (o.getClass() == Integer.class)
					values.put(key,(Integer)o);
				else if (o.getClass() == Boolean.class)
					values.put(key,(Boolean)o);
				else if (o.getClass() == Float.class)
					values.put(key,(Float)o);
				else if (o.getClass() == Double.class)
					values.put(key,(Double)o);
				else
					values.put(key,o.toString());
		}
		return values;
	}

    private static Calendar calculationCalendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));

    /**
     * Convert a filename for an image to a full image Uri.
     * @param imageName filename of the image (like "someImage123.jpg")
     * @return Full Uri of the image in the app folder.
     */
    public static Uri imageNameToUri(Context context, String imageName)
    {
		File imageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
		if(imageDir != null )
			return Uri.parse("file://" + imageDir.getPath() + File.separator + imageName);
        return null;
    }

	public static boolean storageAvailable()
	{
		return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
	}
	private static boolean visible = false;
	public static void setVisible()
	{
		visible = true;
	}
	public static void setInvisible()
	{
		visible = false;
	}
	public static boolean isVisible()
	{
		return visible;
	}

}
