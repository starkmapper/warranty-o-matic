package nl.mapper.warranty_o_matic;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceActivity;

import nl.mapper.warranty_o_matic.data.Util;
import nl.mapper.warranty_o_matic.data.cloud.CloudUtils;

/**
 * Created by stark on 08/06/15.
 */
public class BasePreferenceActivity extends PreferenceActivity
{
	Activity me;
	@Override
	protected void onResume()
	{
		super.onResume();
		me = this;
		Util.setVisible();
		CloudUtils.registerAuthReceiver(this, receiver);

	}

	@Override
	protected void onPause()
	{
		super.onPause();
		Util.setInvisible();
		CloudUtils.unregisterAuthReceiver(this, receiver);
	}
	BroadcastReceiver receiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			SettingsActivity.startForAuth(me);
		}
	};
}
