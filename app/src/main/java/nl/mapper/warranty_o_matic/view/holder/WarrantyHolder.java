package nl.mapper.warranty_o_matic.view.holder;

import android.view.View;
import android.widget.TextView;

import nl.mapper.warranty_o_matic.R;

/**
 * Helper class to set warranty card values
 */
public class WarrantyHolder extends WarrantyBaseHolder
{
	public final TextView warrantyDate;

	public WarrantyHolder(View view)
	{
		super(view);
		warrantyDate = (TextView) view.findViewById(R.id.warranty_card_warranty_date);
	}
}
