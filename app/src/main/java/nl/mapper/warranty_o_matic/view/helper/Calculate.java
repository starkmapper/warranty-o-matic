package nl.mapper.warranty_o_matic.view.helper;

import android.content.Context;

/**
 * Collection of generic util functions
 */
public class Calculate
{
	public static float dpFromPx(float px, Context context)
	{
		return px / context.getResources().getDisplayMetrics().density;
	}

	public static float pxFromDp(float dp, Context context)
	{
		return dp * context.getResources().getDisplayMetrics().density;
	}
	public static int pxFromDp(int dp, Context context)
	{
		return Math.round(pxFromDp((float)dp, context));
	}
	public static int dpFromPx(int px, Context context)
	{
		return Math.round(dpFromPx((float)px, context));

	}

}
