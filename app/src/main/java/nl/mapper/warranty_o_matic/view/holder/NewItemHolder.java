package nl.mapper.warranty_o_matic.view.holder;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import nl.mapper.warranty_o_matic.R;

/**
 *ViewHolder for new items
 */
public class NewItemHolder extends WarrantyBaseHolder
{
	public final ImageView 	barcodeIcon;
	public final ImageView 	serialIcon;
	public final ImageView 	purchaseDateIcon;
	public final ImageView 	purchaseStoreIcon;
	public final ImageView 	warrantyLeftIcon;
	public final Button   	commitButton;
	public final Button   	cancelButton;
	public final Toolbar    toolbar;

	public NewItemHolder(View view)
	{
		super(view);
        serialIcon 		    = (ImageView) view.findViewById(R.id.new_item_warranty_serial_icon);
		barcodeIcon 	    = (ImageView) view.findViewById(R.id.new_item_warranty_barcode_icon);
		purchaseDateIcon    = (ImageView) view.findViewById(R.id.new_item_date_icon);
		purchaseStoreIcon   = (ImageView) view.findViewById(R.id.new_item_store_icon);
		warrantyLeftIcon    = (ImageView) view.findViewById(R.id.new_item_warranty_years_icon);
		commitButton 	    = (Button) view.findViewById(R.id.button_commit);
		cancelButton	    = (Button) view.findViewById(R.id.button_cancel);
		toolbar 		    = (Toolbar) view.findViewById(R.id.warranty_toolbar);
	}
}