package nl.mapper.warranty_o_matic.view.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import nl.mapper.warranty_o_matic.R;

/**
 * Base for Warranty ViewHolders
 */
public class WarrantyBaseHolder
{
	public final TextView productName;
	public final ImageView productImage;
	public final ImageView receiptIcon;
	public final TextView makeModel;
	public final TextView purchaseDate;
	public final TextView purchaseStore;
	public final TextView serialNumber;
	public final TextView warrantyLeft;

	public WarrantyBaseHolder(View view)
	{
		productName = (TextView) view.findViewById(R.id.warranty_card_name);
		makeModel = (TextView) view.findViewById(R.id.warranty_card_make_model);
		purchaseDate = (TextView) view.findViewById(R.id.warranty_card_purchase_date);
		purchaseStore = (TextView) view.findViewById(R.id.warranty_card_purchase_store);
		serialNumber = (TextView) view.findViewById(R.id.warranty_card_serial);
		warrantyLeft = (TextView) view.findViewById(R.id.warranty_card_days_left);

		productImage = (ImageView) view.findViewById(R.id.warranty_card_product_image);
		receiptIcon = (ImageView) view.findViewById(R.id.warranty_card_receipt);

	}
}
