package nl.mapper.warranty_o_matic.view.helper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.util.LruCache;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

import nl.mapper.warranty_o_matic.R;

/**
 * Collection of helper functions to asynchronously load images into ImageViews.
 * Inspired by:
 * https://developer.android.com/training/displaying-bitmaps/load-bitmap.html
 * and:
 * https://developer.android.com/training/displaying-bitmaps/process-bitmap.html
 * and:
 * https://developer.android.com/training/displaying-bitmaps/cache-bitmap.html
 */
public class ImageLoader
{
	/**
	 * Asynchronously loads an image into an ImageView, using the specified placeholder
	 * @param view view to set image on once it is loaded
	 * @param imageUri Uri to load the image from
	 */
	public static void loadImage(ImageView view, Uri imageUri, Context context)
	{
		int imageLoadWidth 	= getValueFromDimension(R.dimen.image_loader_default_width, context);
		int imageLoadHeight = getValueFromDimension(R.dimen.image_loader_default_height, context);

		ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
		if (layoutParams != null)
		{
			if(layoutParams.height != ViewGroup.LayoutParams.MATCH_PARENT)
				imageLoadHeight = layoutParams.height;
			if(layoutParams.width != ViewGroup.LayoutParams.MATCH_PARENT)
				imageLoadWidth = layoutParams.width;
		}
		loadImage(view, imageUri, context.getResources(), R.drawable.image, imageLoadWidth, imageLoadHeight);
	}
	private static int getValueFromDimension(int id, Context context)
	{
		int out = Math.round(context.getResources().getDimension(id));
		if(out == 0)
			out = Calculate.dpFromPx(500, context);

		return out;
	}
	public static void loadImage(ImageView view, Uri imageUri, Resources res, int placeHolderID, int width, int height)
	{
        initCaches();
		if(cancelLoaderTask(view,imageUri))
		{
            Bitmap cachedBitmap = null;
			if (imageUri != null)
				cachedBitmap = bitmapMemoryCache.get(imageUri.hashCode());

            if(cachedBitmap != null)
                view.setImageBitmap(cachedBitmap);
            else
            {
				Bitmap placeHolder = placeHolderCache.get(placeHolderID);
				if(placeHolder == null)
				{
					placeHolder = loadPlaceHolder(res, placeHolderID);
					placeHolderCache.put(placeHolderID,placeHolder);
				}

                final BitmapLoaderTask loaderTask = new BitmapLoaderTask(view, width, height);
                final AsyncDrawable drawable = new AsyncDrawable(res, placeHolder, loaderTask);
                view.setImageDrawable(drawable);
                loaderTask.execute(imageUri);
            }
		}
	}
	private static Bitmap loadPlaceHolder(Resources res, int placeHolderID)
	{
		return BitmapFactory.decodeResource(res,placeHolderID);
	}
    //
    private static void initCaches()
    {
        if(bitmapMemoryCache == null)
        {
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory());
            final int cacheSize = maxMemory / 4;
            final int placeHolderCacheSize = cacheSize /8;
            final int bitmapCacheSize = cacheSize - placeHolderCacheSize;
            bitmapMemoryCache = new LruCache<Integer,Bitmap>(bitmapCacheSize){
                @Override
                protected int sizeOf(Integer key, Bitmap value)
                {
                    return calculateBitmapSize(value);
                }
            };
            placeHolderCache = new LruCache<Integer,Bitmap>(bitmapCacheSize){
                @Override
                protected int sizeOf(Integer key, Bitmap value)
                {
                    return calculateBitmapSize(value);
                }
            };

        }
    }
    private static int calculateBitmapSize(Bitmap bitmap)
    {
        // allocatedByteCount was added in API level 19 (HONEYCOMB_MR1), and is the only reliable way to get actual bytes used
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            return bitmap.getAllocationByteCount();
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB)
            return bitmap.getByteCount();
        return bitmap.getHeight()*bitmap.getRowBytes();
    }
    private static LruCache<Integer, Bitmap> bitmapMemoryCache;
    private static LruCache<Integer,Bitmap> placeHolderCache;

	/**
	 * Cancel any running tasks on the provided ImageView so that a new Uri can be loaded into it.
	 *
	 * @param view View that potentially holds an AsyncDrawable
	 * @param uri Uri that is to be loaded after this task is cancelled
	 * @return true if there are no running tasks left
	 */
	private static boolean cancelLoaderTask(ImageView  view, Uri uri)
	{
		BitmapLoaderTask loaderTask = getLoaderTask(view);
		// only cancel work if it is pointless, or the same uri is being loaded.
		if(loaderTask != null && loaderTask.data!= null)
			if(loaderTask.data.toString().equals("") || loaderTask.data.equals(uri))
			loaderTask.cancel(true);
		else
			return false;

		return true;
	}

	private static BitmapLoaderTask getLoaderTask( ImageView view)
	{
		if(view != null)
		{
			final Drawable drawable= view.getDrawable();
			if(drawable!=null && drawable instanceof AsyncDrawable)
			{
				return ((AsyncDrawable)drawable).getLoaderTask();
			}
		}
		return null;
	}


	public static class AsyncDrawable extends BitmapDrawable
	{
		private final WeakReference<BitmapLoaderTask> loaderTask;
		public AsyncDrawable(Resources res, Bitmap bitmap, BitmapLoaderTask task)
		{
			super(res, bitmap);
			loaderTask = new WeakReference<BitmapLoaderTask>(task);
		}
		BitmapLoaderTask getLoaderTask()
		{
			if(loaderTask!= null)
				return loaderTask.get();
			return null;
		}
	}
	public static class BitmapLoaderTask extends AsyncTask<Uri, Void, Bitmap>
	{
		private final WeakReference<ImageView> view;
		private int imageLoadWidth;
		private int imageLoadHeight;
		private Uri data;
		public BitmapLoaderTask(ImageView imageView, int width, int height)
		{
			imageLoadWidth 	= width;
			imageLoadHeight = height;
			view = new WeakReference<ImageView>(imageView);
		}

		@Override
		protected Bitmap doInBackground(Uri... params)
		{
			data = params[0];
			Bitmap out = null;

			// TODO find a way to calculate the ImageView would have, when the full image would be loaded. Perhaps use meaure function of ImageView?. Or reload thumbnail after setting Bitmap.

			if(imageLoadHeight <= 0 && imageLoadWidth <= 0)
			{
				out = loadBitmapFromUri(data);
			}
			else
			{
				out = loadScaledBitmapFromUri(data, imageLoadWidth, imageLoadHeight);
			}
            if(out != null)
                bitmapMemoryCache.put(data.hashCode(),out);
			return out;
		}

		protected Bitmap loadScaledBitmapFromUri(Uri uri, int width, int height)
		{
			if(uri == null)
				return null;

			String scheme = uri.getScheme();
			if(scheme != null)
			{
				// set options to just get the bounds of the bitmap;
				BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
				bitmapOptions.inJustDecodeBounds = true;
				// local file
				if(scheme.equals("file"))
				{
					String filePath = uri.getPath();
					BitmapFactory.decodeFile(filePath,bitmapOptions);
					int bitmapWidth = bitmapOptions.outWidth;
					int bitmapHeight = bitmapOptions.outHeight;

					// ignore wrap_content or match_parent values when calculating sample size
					if(height <= 0)
						height = bitmapHeight;
					if(width<=0)
						width = bitmapWidth;

					// Calculate the sample size for the requested bitmap so that it fits withing the specified width and height
					bitmapOptions.inSampleSize = calculateSampleSize(bitmapWidth,bitmapHeight,width,height);
					// Actually load bitmap data this time
					bitmapOptions.inJustDecodeBounds = false;
                    Bitmap bitmap = BitmapFactory.decodeFile(filePath,bitmapOptions);
					if(bitmap != null)
						return ImageUtil.autoRotateBitmap(bitmap,filePath);
				}
			}
			return null;
		}
		protected int calculateSampleSize(int width,int height, int desiredWidth, int desiredHeight)
		{
			//
			int sampleSize = 1;
			if(width > desiredWidth || height > desiredHeight)
			{
				final int halfHeight = height / 2;
				final int halfWidth = width / 2;

				// Calculate the largest inSampleSize value that is a power of 2 and keeps both
				// height and width larger than the requested height and width.
				while ((halfHeight / sampleSize) > desiredHeight
					&& (halfWidth / sampleSize) > desiredWidth) {
					sampleSize *= 2;
				}
			}
			return sampleSize;
		}
		protected Bitmap loadBitmapFromUri(Uri uri)
		{
			if(uri == null)
				return null;
			String scheme = uri.getScheme();
			if(scheme != null)
			{
				// local file
				if (scheme.equals("file"))
				{
					return BitmapFactory.decodeFile(uri.getPath());
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap)
		{
			if(isCancelled())
				bitmap = null;
			if(view!= null && bitmap != null)
			{
				final ImageView imageView = view.get();
				final BitmapLoaderTask task = getLoaderTask(imageView);
				if (this == task && imageView!= null)
					imageView.setImageBitmap(bitmap);
			}
		}
	}

}
