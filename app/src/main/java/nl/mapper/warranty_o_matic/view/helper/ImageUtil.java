package nl.mapper.warranty_o_matic.view.helper;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;

import java.io.IOException;

/**
 * Created by mwstappe on 9-4-2015.
 * Inspired by http://stackoverflow.com/a/11081918
 */
public class ImageUtil
{
    private static final String LOG_TAG = ImageUtil.class.getSimpleName();
    public static Bitmap autoRotateBitmap(Bitmap source, String filePath)
    {
        int rotation = getBitmapOrientation(filePath);
        return rotateBitmap(source,rotation);
    }

    public static Bitmap rotateBitmap(Bitmap source, int rotation)
    {
        Matrix matrix = new Matrix();
        matrix.preRotate(rotation,source.getWidth()/2,source.getHeight()/2);
        return Bitmap.createBitmap(source,0,0,source.getWidth(), source.getHeight(), matrix,false);
    }

    /**
     * Reads orientation from an image file
     * @param filePath path to the file to read exif data from
     * @return image orientation in degrees 0-360
     */
    public static int getBitmapOrientation(String filePath)
    {
        try
        {
            ExifInterface exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            return exifOrientationToDegrees(orientation);
        }
        catch (IOException e)
        {
            Log.d(LOG_TAG,"Error reading file ",e);
        }
        return 0;
    }
    public static int exifOrientationToDegrees(int exifOrientation)
    {
        switch (exifOrientation)
        {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return 90;
            case ExifInterface.ORIENTATION_ROTATE_180:
                return 180;
            case ExifInterface.ORIENTATION_ROTATE_270:
                return 270;
        }
        return 0;
    }
}
