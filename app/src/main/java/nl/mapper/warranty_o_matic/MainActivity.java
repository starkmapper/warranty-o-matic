package nl.mapper.warranty_o_matic;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.audiofx.BassBoost;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.widget.SearchView;

import nl.mapper.warranty_o_matic.data.Preferences;
import nl.mapper.warranty_o_matic.data.sync.SyncAdapter;


public class MainActivity extends BaseActivity
{



	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		View newItemButton =  findViewById(R.id.main_new_item_button);
		if(newItemButton!=null)
			newItemButton.setOnClickListener(new View.OnClickListener()
											 {
												 @Override
												 public void onClick(View v)
												 {
													 Context context = v.getContext();
													 Intent newItemIntent = new Intent(context, NewItemActivity.class);
													 startActivity(newItemIntent);
												 }
											 }

			);

		setSearchQueryFromIntent(getIntent());
		if (Preferences.syncToDrive(this))
			SyncAdapter.initializeSync(this);

		me = this;

	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		setSearchQueryFromIntent(intent);
		super.onNewIntent(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		MenuItem item = menu.findItem(R.id.action_search);
		MenuItemCompat.setOnActionExpandListener(item,new MenuItemCompat.OnActionExpandListener()
			{
				@Override
				public boolean onMenuItemActionExpand(MenuItem item)
				{
					return true;
				}

				@Override
				public boolean onMenuItemActionCollapse(MenuItem item)
				{
					setSearchQuery(null);
					return true;
				}
			});

		SearchView    searchView 	= (SearchView) MenuItemCompat.getActionView(item);
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setOnCloseListener(new SearchView.OnCloseListener()
		{
			@Override
			public boolean onClose()
			{
				setSearchQuery(null);
				return false;
			}
		});
		return true;
	}
	@Override
	protected void onResume()
	{
		super.onResume();
		if(shouldAskToSyncToDrive())
			askToSyncToDrive();
	}

	private void askToSyncToDrive()
	{
		// Show chooser dialog
		Dialog driveDialog = getDriveDialog(this);
		driveDialog.show();
	}

	private void setSearchQueryFromIntent(Intent intent)
	{
		if(intent != null && Intent.ACTION_SEARCH.equals(intent.getAction()))
		{
			String query = intent.getStringExtra(SearchManager.QUERY);
			setSearchQuery(query);
		}
	}
	private void setSearchQuery(String query)
	{
		WarrantyFragment fragment = (WarrantyFragment) getSupportFragmentManager().findFragmentById(R.id.main_fragment_warranty);
		if(fragment != null)
			fragment.setSearchQuery(query);
	}

	private boolean shouldAskToSyncToDrive()
	{
		return !Preferences.driveConfigured(this);
	}

	DialogInterface.OnClickListener driveOnClickListener = new DialogInterface.OnClickListener()
	{
		@Override
		public void onClick(DialogInterface dialog, int which)
		{
			if (which != DialogInterface.BUTTON_NEUTRAL)
			{
				boolean enable = which == DialogInterface.BUTTON_POSITIVE;
				Preferences.setSyncToDrive(me, enable);
				if (enable)
				{
					// Start the settings Activity to setup The Cloud
					Intent intent = new Intent(me, SettingsActivity.class);
					intent.setAction(SettingsActivity.AUTH_CLOUD_ACTION);
					startActivity(intent);
				}
			}
		}
	};

	Activity me;

	private Dialog getDriveDialog(Context context)
	{
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
		dialogBuilder.setTitle(R.string.drive_dialog_title)
				.setMessage(R.string.drive_dialog_message)
				.setNegativeButton(getString(R.string.no),driveOnClickListener)
				.setPositiveButton(getString(R.string.yes), driveOnClickListener)
				.setNeutralButton(R.string.drive_dialog_later, driveOnClickListener);
		return dialogBuilder.create();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		switch (item.getItemId())
		{
			case R.id.action_sync:
				Preferences.setForceSync(this, true);
				SyncAdapter.syncNow(this);

				return true;
			case R.id.action_settings:
				Intent intent = new Intent(this, SettingsActivity.class);
				startActivity(intent);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
