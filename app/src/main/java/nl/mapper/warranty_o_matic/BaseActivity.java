package nl.mapper.warranty_o_matic;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import nl.mapper.warranty_o_matic.data.Util;
import nl.mapper.warranty_o_matic.data.cloud.CloudUtils;

/**
 * Base activity to allow the syncAdapter to know whether the app is running in foreground.
 * http://stackoverflow.com/a/10094553/4787469
 */

public class BaseActivity extends AppCompatActivity
{
	Activity me;
	@Override
	protected void onResume()
	{
		super.onResume();
		Util.setVisible();
		me = this;
		CloudUtils.registerAuthReceiver(this, receiver);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		Util.setInvisible();
		CloudUtils.unregisterAuthReceiver(this, receiver);
	}
	BroadcastReceiver receiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			SettingsActivity.startForAuth(me);
		}
	};
}
