package nl.mapper.warranty_o_matic;

import android.database.Cursor;
import android.os.Build;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.CursorLoader;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;

import nl.mapper.warranty_o_matic.data.WarrantyContract.WarrantyInfo;


/**
 * A simple {@link Fragment} subclass. This
 */
public class WarrantyFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>
{


	public void setSearchQuery(String query)
	{
		searchQuery = query;
		getLoaderManager().restartLoader(WARRANTY_LOADER_ID, null, this);
	}

	WarrantyAdapter adapter;
	Cursor dataCursor;
	String searchQuery;
	public WarrantyFragment()
	{
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_warranty, container, false);
		adapter = new WarrantyAdapter(getActivity(),null,0);
		listGridUnit = (GridView) view.findViewById(R.id.gridview_warranty);
		listGridUnit.setAdapter(adapter);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
		{
			listGridUnit.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener()
			{
				@Override
				public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked)
				{
				}

				@Override
				public boolean onCreateActionMode(ActionMode mode, Menu menu)
				{
					setContextMenu(menu);
					return true;
				}

				@Override
				public boolean onPrepareActionMode(ActionMode mode, Menu menu)
				{
					return false;
				}

				@Override
				public boolean onActionItemClicked(ActionMode mode, MenuItem item)
				{
					return onContextItemSelected(item);
				}

				@Override
				public void onDestroyActionMode(ActionMode mode)
				{

				}

			});
		}
		else
		{
			registerForContextMenu(listGridUnit);
		}
		setCursor();
		return view;
	}
	private void deleteCheckedItems(GridView view)
	{

		long[] IDs;
		if(Build.VERSION.SDK_INT  < Build.VERSION_CODES.HONEYCOMB)
		{
			IDs = new long[1];
			IDs[0] = view.getItemIdAtPosition(contextMenuPos);
		}
		else
		{
			IDs = view.getCheckedItemIds();
		}
		CleanupService.startDeleteWarrantyItems(getActivity(), IDs);
	}
	private void setContextMenu(Menu menu)
	{
		MenuInflater inflater = getActivity().getMenuInflater();
		inflater.inflate(R.menu.menu_warranty_card,menu);
	}
	int contextMenuPos;

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, v, menuInfo);
		setContextMenu(menu);
		if(menuInfo instanceof AdapterView.AdapterContextMenuInfo)
			contextMenuPos = ((AdapterView.AdapterContextMenuInfo) menuInfo).position;
	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.action_delete:
				deleteCheckedItems(listGridUnit);
				return true;
		}
		return super.onContextItemSelected(item);
	}

	private GridView listGridUnit;
	private static final int WARRANTY_LOADER_ID = 789123;
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		getLoaderManager().initLoader(WARRANTY_LOADER_ID, savedInstanceState, this);
	}

	public void reloadData()
	{
		getLoaderManager().restartLoader(WARRANTY_LOADER_ID, null, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args)
	{
		// Put first to expire item at the top of the list
		String sortString = WarrantyInfo.COlUMN_WARRANTY_DATE + " ASC";
		Uri warrantyListUri = WarrantyInfo.CONTENT_URI;
		if(!TextUtils.isEmpty(searchQuery))
			warrantyListUri = WarrantyInfo.buildUriWithTextSearch(searchQuery);
		return new CursorLoader(getActivity(),warrantyListUri, WarrantyAdapter.WARRANTY_PROJECTION,null,null,sortString);
	}
	private void setCursor()
	{
		if(adapter != null && dataCursor!= null)
			adapter.swapCursor(dataCursor);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data)
	{
		dataCursor = data;
		setCursor();
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader)
	{
		adapter.swapCursor(null);
	}
}
