package nl.mapper.warranty_o_matic;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceActivity;
import android.preference.SwitchPreference;
import android.widget.Toast;

import nl.mapper.warranty_o_matic.data.Preferences;
import nl.mapper.warranty_o_matic.data.cloud.StorageProvider;
import nl.mapper.warranty_o_matic.data.cloud.providers.GoogleDrive;
import nl.mapper.warranty_o_matic.data.sync.SyncAdapter;

/**
 * Simple activity to set some basic preferences.
 */
public class SettingsActivity extends BasePreferenceActivity implements StorageProvider.ConnectionCallbacks, SharedPreferences.OnSharedPreferenceChangeListener
{
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
	{
		if (key.equals(getString(R.string.pref_sync_to_drive)))
		{
			Preferences.setSyncToDriveConfigured(this);
			// Have sync setup when it was just (re) enabled
			if (Preferences.syncToDrive(this))
			{
				SyncAdapter.enable(this);
			}
			else
			{
				SyncAdapter.disable(this);
			}
		}
	}

	public static void startForAuth(Context context)
	{
		Intent intent = new Intent(context, SettingsActivity.class);
		intent.setAction(AUTH_CLOUD_ACTION);
		context.startActivity(intent);
	}

	public static final String AUTH_CLOUD_ACTION = "auth_cloud";
	private static final int AUTH_REQUEST_CODE = 12;

	@Override
	public void onConnectionFailed(StorageProvider.ConnectionResult problem)
	{
		problem.fix(this,AUTH_REQUEST_CODE);
	}

	boolean disableSync = false;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == AUTH_REQUEST_CODE)
		{
			// Auth cancelled by user, disable cloud sync
			if (resultCode == 0)
			{
				disableSync = true;
			}
			else // Try connecting again
				cloudStorage.connectAsync(this);
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		getPreferenceScreen().getSharedPreferences()
				.registerOnSharedPreferenceChangeListener(this);
		if (disableSync)
		{
			disableSync = false;
			Preferences.setSyncToDrive(this, false);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
			{
				SwitchPreference drivePref = (SwitchPreference) findPreference(getString(R.string.pref_sync_to_drive));
				drivePref.setChecked(false);
			}
			else
			{
				CheckBoxPreference drivePref = (CheckBoxPreference) findPreference(getString(R.string.pref_sync_to_drive));
				drivePref.setChecked(false);
			}
		}
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		getPreferenceScreen().getSharedPreferences()
				.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onConnected()
	{
		Toast.makeText(this, this.getString(R.string.message_cloud_connected), Toast.LENGTH_LONG).show();
		cloudStorage.disconnect();
		cloudStorage = null;
		// Initiate sync now that we got authentication out of the way.
		SyncAdapter.syncNow(this);
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		handleIntent(intent);
	}
	private void handleIntent(Intent intent)
	{
		if (intent != null)
			if (AUTH_CLOUD_ACTION.equals(intent.getAction()))
				authenticateTheCloud();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.prefs);
		handleIntent(getIntent());
	}
	StorageProvider cloudStorage;
	private void authenticateTheCloud()
	{
		cloudStorage = new GoogleDrive(this);
		cloudStorage.connectAsync(this);
	}
}
