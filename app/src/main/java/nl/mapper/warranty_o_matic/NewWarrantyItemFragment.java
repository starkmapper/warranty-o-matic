package nl.mapper.warranty_o_matic;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import nl.mapper.warranty_o_matic.data.Util;
import nl.mapper.warranty_o_matic.data.WarrantyContract.WarrantyInfo;

import nl.mapper.warranty_o_matic.data.WarrantyContract;
import nl.mapper.warranty_o_matic.view.helper.ImageLoader;
import nl.mapper.warranty_o_matic.view.holder.NewItemHolder;

/**
 * Allows user to add a new warranty item
 */
public class NewWarrantyItemFragment extends Fragment
{

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_new_warranty_item, container, false);
		itemHolder = new NewItemHolder(view);
		itemHolder.purchaseDate.setOnClickListener(dateClickListener);
		itemHolder.purchaseDate.setOnFocusChangeListener(dateFocusListener);
		itemHolder.productImage.setOnClickListener(getImageClickListener);
		itemHolder.receiptIcon.setOnClickListener(getImageClickListener);

		itemHolder.purchaseStore.setOnFocusChangeListener(iconFocusListener);
		itemHolder.warrantyLeft.setOnFocusChangeListener(iconFocusListener);
		itemHolder.serialNumber.setOnFocusChangeListener(iconFocusListener);

		itemHolder.makeModel.setNextFocusDownId(R.id.warranty_card_purchase_date);
        scanIntent = createBarcodeScannerIntent();
        itemHolder.barcodeIcon.setOnClickListener(barcodeScannerClickListener);
		if(itemHolder.cancelButton!= null)
		itemHolder.cancelButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				// Delete any images that were already taken
				cleanupUnusedImages();
				getActivity().finish();
			}
		});

		if(itemHolder.commitButton != null)
			itemHolder.commitButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				addCurrentItemAndExit();
			}
		});

		if (savedInstanceState != null)
		{
			try
			{
				long date = savedInstanceState.getLong(PURCHASE_DATE_KEY);
				if(date > 0)
				{
					purchaseDate.setTimeInMillis(date);
					purchaseDateSet = true;
					updateDateString();
				}

				String prodImageUriString = savedInstanceState.getString(PROD_IMAGE_URI_KEY);
				String receiptImageUriString = savedInstanceState.getString(RECEIPT_URI_KEY);
				String cameraCaptureUriString = savedInstanceState.getString(CAPTURE_URI_KEY);
				syncTime = savedInstanceState.getLong(SYNC_TIME_KEY,0);
				if(prodImageUriString != null)
					setProductImageUri(Uri.parse(prodImageUriString));
				if(receiptImageUriString != null)
					setReceiptImageUri(Uri.parse(receiptImageUriString));
				if(cameraCaptureUriString!= null)
					cameraCaptureUri = Uri.parse(cameraCaptureUriString);
			}
			catch (Exception e)
			{
				Log.e(LOG_TAG, "Error parsing bundle", e);
			}
		}
		else
		{
			syncTime = Util.getSyncTime();
		}

		return view;
	}
    private IntentIntegrator createBarcodeScannerIntent()
    {
        IntentIntegrator out = new IntentIntegrator(this);
        out.setButtonNoByID(android.R.string.no);
        out.setButtonYesByID(android.R.string.yes);
        out.setMessage(getActivity().getString(R.string.install_barcode_scanner_message));
        out.setTitle(getActivity().getString(R.string.install_barcode_scanner_title));
        return out;
    }

	private boolean deleteFromUri(Uri uri)
	{
		CleanupService.startActionDeleteImage(getActivity(),uri);
		return true;
	}
	NewItemHolder itemHolder;
	private static final int PRODUCT_IMAGE_ID = 502;
	private static final int RECEIPT_IMAGE_ID = 503;
	Uri cameraCaptureUri;
	Uri productImageUri;
	Uri receiptUri;
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(scanResult!=null && itemHolder.serialNumber!= null)
        {
            itemHolder.serialNumber.setText(scanResult.getContents());
        }
		else if(resultCode == Activity.RESULT_OK)
		{
            Log.d(LOG_TAG, "Image capture successful");
			switch (requestCode)
			{
				case PRODUCT_IMAGE_ID:
					// Delete previous picture or the SD-card will fill up really really fast
					deleteFromUri(productImageUri);
					setProductImageUri(cameraCaptureUri);
					break;
				case RECEIPT_IMAGE_ID:
					deleteFromUri(receiptUri);
					setReceiptImageUri(cameraCaptureUri);
					break;
			}
		}
        else
        {
            Log.d(LOG_TAG, "Image capture cancelled");
        }
	}

	private void setReceiptImageUri(Uri uri)
	{
		if(uri != null)
		{
			receiptUri = uri;
			// clear color filter
			itemHolder.receiptIcon.setColorFilter(0);
			ImageLoader.loadImage(itemHolder.receiptIcon,null,getResources(),R.drawable.document_full,itemHolder.receiptIcon.getWidth(), itemHolder.receiptIcon.getHeight());
		}
	}

	private void setProductImageUri(Uri uri)
	{
		if(uri!= null)
		{
			productImageUri = uri;
			// clear color filter
			itemHolder.productImage.setColorFilter(0);
			ImageLoader.loadImage(itemHolder.productImage,productImageUri,getActivity());
		}
	}

	int getRequestCodeFromView(View v)
	{
		switch(v.getId())
		{
			case R.id.warranty_card_product_image:
				return PRODUCT_IMAGE_ID;
			case R.id.warranty_card_receipt:
				return RECEIPT_IMAGE_ID;
		}
		return 0;
	}


	private void startCameraIntent(int viewID)
	{
		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		cameraCaptureUri = getPictureOutputUri(viewID == RECEIPT_IMAGE_ID);
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraCaptureUri);
		if(cameraIntent.resolveActivity(getActivity().getPackageManager()) != null)
			startActivityForResult(cameraIntent, viewID);
		else
			Toast.makeText(getActivity(),"No camera available", Toast.LENGTH_LONG).show();
	}
	private View.OnClickListener getImageClickListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			startCameraIntent(getRequestCodeFromView(v));
		}
	};
    IntentIntegrator scanIntent;

    private View.OnClickListener barcodeScannerClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            scanIntent.initiateScan();
        }
    };

	// TODO: find a way to NOT popup the date picker when date field is focused, but picker is not shown, and device is rotated
	private void showDatePicker()
	{
		Calendar today = Calendar.getInstance();
		//show dialog
		DatePickerDialog dialog = new DatePickerDialog(getActivity(),dateSetListener,today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH));
		dialog.show();
	}
	private ImageView getIconFromTextView(View v)
	{
		if (v == itemHolder.purchaseDate)
			return itemHolder.purchaseDateIcon;
		else if (v == itemHolder.purchaseStore)
			return itemHolder.purchaseStoreIcon;
		else if (v == itemHolder.warrantyLeft)
			return itemHolder.warrantyLeftIcon;

		return itemHolder.serialIcon;
	}

	private View.OnFocusChangeListener iconFocusListener = new ValidatedFocusListener();
	private View.OnFocusChangeListener dateFocusListener = new ValidatedFocusListener()
	{
		@Override
		public void onFocusChange(View v, boolean hasFocus)
		{
			if (hasFocus)
				showDatePicker();
			super.onFocusChange(v,hasFocus);
		}
	};
	class ValidatedFocusListener implements View.OnFocusChangeListener
	{
		@Override
		public void onFocusChange(View v, boolean hasFocus)
		{
			if (!hasFocus)
				removeIconTintIfNotEmpty(v);
		}
	}
	private void updateIconTints()
	{
		removeIconTintIfNotEmpty(itemHolder.purchaseDate);
		removeIconTintIfNotEmpty(itemHolder.purchaseStore);
		removeIconTintIfNotEmpty(itemHolder.warrantyLeft);
		removeIconTintIfNotEmpty(itemHolder.serialNumber);
	}
	private void removeIconTintIfNotEmpty(View v)
	{
		if (((TextView)v).getText().length()>0)
			getIconFromTextView(v).setColorFilter(getActivity().getResources().getColor(R.color.warranty_icon_correct));
	}

	private View.OnClickListener dateClickListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			showDatePicker();
		}
	};
	private Calendar purchaseDate = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
	private boolean purchaseDateSet = false;
	private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener()
	{
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
		{
			setDate(year,monthOfYear,dayOfMonth);
			View v = itemHolder.purchaseDate.focusSearch(View.FOCUS_DOWN);
			if (v!= null)
				v.requestFocus();
		}
	};

	private void updateDateString()
	{
		itemHolder.purchaseDate.setText(WarrantyAdapter.formatDate(purchaseDate));
	}

	private void setDate(int year,  int monthOfYear, int dayOfMonth)
	{
		purchaseDate.set(year, monthOfYear, dayOfMonth);
		purchaseDateSet = true;
		updateDateString();

	}

	private final static String PROD_IMAGE_URI_KEY 	= "ProductImage";
	private final static String RECEIPT_URI_KEY 	= "ReceiptImage";
	private final static String CAPTURE_URI_KEY 	= "CapturedImage";
	private final static String PURCHASE_DATE_KEY 	= "PurchaseDate";
	private final static String SYNC_TIME_KEY       = "SyncTime";

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		if(productImageUri != null)
			outState.putString(PROD_IMAGE_URI_KEY,productImageUri.toString());
		if(receiptUri != null)
			outState.putString(RECEIPT_URI_KEY, receiptUri.toString());
		if(cameraCaptureUri!= null)
			outState.putString(CAPTURE_URI_KEY, cameraCaptureUri.toString());
		if(purchaseDateSet)
			outState.putLong(PURCHASE_DATE_KEY, purchaseDate.getTimeInMillis());

		outState.putLong(SYNC_TIME_KEY,syncTime);
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
			case R.id.action_add:
				addCurrentItemAndExit();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_fragment_new, menu);
	}

	private long addWarrantyItem(NewItemHolder holder)
	{
		ContentValues values = viewHolderToContentValues(holder);
		ContentResolver resolver = getActivity().getContentResolver();

		Uri insertedUri = resolver.insert(WarrantyContract.WarrantyInfo.CONTENT_URI, values);
		return ContentUris.parseId(insertedUri);
	}


	String calendarToUnixTimeString(Calendar cal)
	{
		return Long.toString(cal.getTime().getTime()/1000);
	}

	private ContentValues viewHolderToContentValues(NewItemHolder holder)
	{
		ContentValues out = new ContentValues();
		String nameString 			= holder.productName.getText().toString();
		String purchaseStore      	= holder.purchaseStore.getText().toString();

		String warrantyYearsString  = holder.warrantyLeft.getText().toString();

		String serialString 		= holder.serialNumber.getText().toString();
		String makeModel            = holder.makeModel.getText().toString();

		int firstSpace = makeModel.indexOf(" ");
		String make;
		String model;
		if(firstSpace != -1)
		{
			make = makeModel.substring(0, firstSpace);
			model = makeModel.substring(firstSpace);
		}
		else
		{
			make = makeModel;
			model = "";
		}

		// Convert to unix time. Don't care about timezones as it is a date.
		String normalizedPurchaseDate = calendarToUnixTimeString(purchaseDate);

		Calendar warrantyDate = (Calendar)purchaseDate.clone();
		warrantyDate.add(Calendar.YEAR, Integer.parseInt(warrantyYearsString));
		String warrantyExpireDate = calendarToUnixTimeString(warrantyDate);


		out.put( WarrantyInfo.COLUMN_FRIENDLY_NAME, nameString);

		out.put( WarrantyInfo.COlUMN_PURCHASE_DATE, normalizedPurchaseDate);
		out.put( WarrantyInfo.COlUMN_WARRANTY_DATE, warrantyExpireDate);
		out.put( WarrantyInfo.COlUMN_STORE, purchaseStore);
		out.put( WarrantyInfo.COlUMN_SERIAL,serialString);
		out.put( WarrantyInfo.COlUMN_MAKE, make);
		out.put( WarrantyInfo.COlUMN_MODEL, model);

		out.put( WarrantyInfo.COlUMN_IMAGE_URI, productImageUri.getLastPathSegment());
		out.put( WarrantyInfo.COlUMN_RECEIPT_URI, receiptUri.getLastPathSegment());

		// syncTime of "0" indicates a new item
		out.put( WarrantyInfo.COLUMN_SYNC_ID, syncTime);
		out.put( WarrantyInfo.COLUMN_SYNC_TIME, 0);

		return out;
	}

	private Uri getPictureOutputUri(boolean isReceipt)
	{
		return Uri.fromFile(getPictureOutputFile(isReceipt));
	}
	private long syncTime;
	private File getPictureOutputFile(boolean isReceipt)
	{

		String formatString = getString(isReceipt ? R.string.format_filename_receipt:R.string.format_filename_image);
		String fileName = String.format(formatString, syncTime);
		Uri imageUri = Util.imageNameToUri(getActivity(),fileName);
		if (imageUri != null && Util.storageAvailable() )
			return new File(imageUri.getPath());
		return null;
	}

	public void cleanupUnusedImages()
	{
		CleanupService.startDeleteUnusedImages(getActivity());
	}

	public void addCurrentItemAndExit()
	{
		try
		{
			long id = addWarrantyItem(itemHolder);
			if(id > 0)
				((NewItemCallback)getActivity()).OnNewItemAdded(id);
			getActivity().setResult(Activity.RESULT_OK);
			getActivity().finish();
		}
		catch (Exception e)
		{
			Log.e(LOG_TAG, "Error parsing fields", e);
			Toast.makeText(getActivity(), getActivity().getString(R.string.toast_error_parsing_data), Toast.LENGTH_LONG).show();
		}
	}
	private static final String LOG_TAG = NewWarrantyItemFragment.class.getSimpleName();
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	/**
	 * All activities using this fragment should implement these callbacks.
	 */
	public interface NewItemCallback
	{
		/**
		 * Notifies the attached activity when a new item was added to the database
		 * @param itemID ID of the newly added item
		 */
		void OnNewItemAdded(long itemID);
	}

}
