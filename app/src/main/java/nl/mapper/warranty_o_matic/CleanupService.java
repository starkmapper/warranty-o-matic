package nl.mapper.warranty_o_matic;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FilenameFilter;
import java.util.LinkedList;

import nl.mapper.warranty_o_matic.data.Util;
import nl.mapper.warranty_o_matic.data.WarrantyContract.WarrantyInfo;
import nl.mapper.warranty_o_matic.data.WarrantyProvider;
import nl.mapper.warranty_o_matic.data.sync.SyncAdapter;

/**
 * Helper service for deleting warranty items, and cleaning up (unused) images.
 */
public class CleanupService extends IntentService
{
	private static final String ACTION_DELETE_IMAGE 			= "nl.mapper.warranty_o_matic.action.DELETE_IMAGE";
	private static final String ACTION_DELETE_WARRANTY_ITEM 	= "nl.mapper.warranty_o_matic.action.DELETE_WARRANTY_ITEM";
	private static final String ACTION_DELETE_WARRANTY_ITEMS 	= "nl.mapper.warranty_o_matic.action.DELETE_WARRANTY_ITEMS";
	private static final String ACTION_DELETE_UNUSED_ITEMS 		= "nl.mapper.warranty_o_matic.action.DELETE_UNUSED_ITEMS";

	private static final String EXTRA_IDS = "nl.mapper.warranty_o_matic.extra.IDs";

	/**
	 * Start the service with an intent to delete an image. At some point this image will be checked for use in the database.
	 * If the image is still in use, deletion will nog occur.
	 * @param context Context
	 * @param image Uri to the image to delete. Only file: format supported.
	 */
	public static void startActionDeleteImage(Context context, Uri image)
	{
		Intent intent = new Intent(context, CleanupService.class);
		intent.setAction(ACTION_DELETE_IMAGE);
		intent.setData(image);
		context.startService(intent);
	}

	/**
	 * Starts the service with an intent to delete a warranty item and all it's data. This includes any images or thumbnails written to disk.
	 * @param context Context
	 * @param warrantyUri Uri pointing to a WarrantyItem in the database
	 */
	public static void startDeleteWarrantyItem(Context context, Uri warrantyUri)
	{
		Intent intent = new Intent(context, CleanupService.class);
		intent.setAction(ACTION_DELETE_WARRANTY_ITEM);
		intent.setData(warrantyUri);
		context.startService(intent);
	}
	public static void startDeleteWarrantyItems(Context context, long[] IDs)
	{
		Intent intent = new Intent(context, CleanupService.class);
		intent.setAction(ACTION_DELETE_WARRANTY_ITEMS);
		intent.putExtra(EXTRA_IDS, IDs);
		context.startService(intent);
	}


	public static void startDeleteUnusedImages(Context context)
	{
		Intent intent = new Intent(context, CleanupService.class);
		intent.setAction(ACTION_DELETE_UNUSED_ITEMS);
		context.startService(intent);
	}

	public CleanupService()
	{
		super("CleanupService");
	}

	@Override
	protected void onHandleIntent(Intent intent)
	{
		if (intent != null)
		{
			final String action = intent.getAction();
			switch (action)
			{
				case ACTION_DELETE_IMAGE:
					final Uri imageUri = intent.getData();
					handleActionDeleteImage(imageUri);
					break;
				case ACTION_DELETE_WARRANTY_ITEM:
					final Uri warrantyItemUri = intent.getData();
					handleActionDeleteWarrantyItem(warrantyItemUri);
					break;
				case ACTION_DELETE_WARRANTY_ITEMS:
					long[] IDs = intent.getExtras().getLongArray(EXTRA_IDS);
					for (long id : IDs)
						handleActionDeleteWarrantyItem(WarrantyInfo.buildWarrantyUri(id));
					break;
				case ACTION_DELETE_UNUSED_ITEMS:
					deleteUnusedImages();
					break;
			}
		}
	}

	private boolean handleActionDeleteImage(Uri imageUri)
	{
		if(imageUri!= null)
		{
			File imageFile = new File(imageUri.getPath());
			return imageFile.delete();
		}
		return false;
	}

	private static final String [] WARRANTY_IMAGES_PROJECTION =
			{
					WarrantyInfo.COlUMN_IMAGE_URI,
					WarrantyInfo.COlUMN_RECEIPT_URI
			};
	private static final String WARRANTY_IMAGES_SELECTION = WarrantyInfo.COlUMN_IMAGE_URI + " LIKE ? OR " + WarrantyInfo.COlUMN_RECEIPT_URI + " LIKE ?";

	private void handleActionDeleteWarrantyItem(Uri warrantyUri)
	{
		ContentResolver resolver = getContentResolver();
		if(resolver != null)
		{
			int affectedRows = 0;
			Cursor imageCursor = resolver.query(warrantyUri,WARRANTY_IMAGES_PROJECTION,null,null,null);
			LinkedList<Uri> imageUris = new LinkedList<>();
			if(imageCursor.moveToFirst())
			{
				// Delete warranty entry first, as usage checks might prevent deleting of images
				affectedRows += resolver.delete(warrantyUri,null,null);
				int count = imageCursor.getColumnCount();
				for(int i = 0; i<count;i++)
				{
					try
					{
						imageUris.add(Util.imageNameToUri(this,imageCursor.getString(i)));
					}
					catch (Exception e)
					{
						Log.e(LOG_TAG, "Error deleting image: " + imageCursor.getString(i), e);
					}
				}
			}
			imageCursor.close();
			// Notify users of changes
			if(affectedRows > 0)
				WarrantyProvider.notify(WarrantyInfo.CONTENT_URI,getApplicationContext());

			// Delete image after notifying users, so reload time is as low as possible
			for(Uri image : imageUris)
			{
				handleActionDeleteImage(image);
			}
			SyncAdapter.syncNow(this);
		}


	}


	private void deleteUnusedImages()
	{
		File imageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
		if(imageDir != null && imageDir.isDirectory())
		{
			final String extentions[] = getResources().getStringArray(R.array.image_extensions);
			File files[] = imageDir.listFiles(new FilenameFilter()
			{
				@Override
				public boolean accept(File dir, String filename)
				{
					for (String extention : extentions)
						if(filename.toLowerCase().endsWith(extention))
							return true;
					return false;
				}
			});
			int deletedFiles = 0;
			for(File file : files)
			{
				if(file.isFile())
				{
					String selectionArgs[] = new String[2];
					for (int i = 0; i < 2; i++)
						selectionArgs[i] = "%"+file.getName();

					Cursor cursor = getContentResolver().query(WarrantyInfo.CONTENT_URI,WARRANTY_IMAGES_PROJECTION,WARRANTY_IMAGES_SELECTION,selectionArgs,null);
					if(!cursor.moveToFirst())
						if (file.delete())
							deletedFiles++;
					cursor.close();
				}
			}
		}
	}
	private static final String LOG_TAG = CleanupService.class.getSimpleName();
}
