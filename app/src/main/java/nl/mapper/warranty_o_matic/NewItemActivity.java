package nl.mapper.warranty_o_matic;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import nl.mapper.warranty_o_matic.data.Preferences;
import nl.mapper.warranty_o_matic.data.WarrantyContract;
import nl.mapper.warranty_o_matic.data.sync.SyncAdapter;


public class NewItemActivity extends BaseActivity implements NewWarrantyItemFragment.NewItemCallback
{


	private static final int AUTHENTICATE_GOOGLE_DRIVE = 123;


	@Override
	public void onBackPressed()
	{
		NewWarrantyItemFragment wf = (NewWarrantyItemFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_new_item);
		wf.cleanupUnusedImages();
		super.onBackPressed();
	}






	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_item);
		Toolbar toolbar = (Toolbar) findViewById(R.id.warranty_toolbar);
		if(toolbar != null)
		{
			setSupportActionBar(toolbar);
			ActionBar bar = getSupportActionBar();
			if (bar != null)
				bar.setDisplayHomeAsUpEnabled(true);
		}
	}

	public boolean shouldSyncToDrive()
	{
		return Preferences.syncToDrive(this);
	}

	@Override
	public void OnNewItemAdded(long itemID)
	{
		if (shouldSyncToDrive())
		{
			Uri uri = WarrantyContract.WarrantyInfo.buildWarrantyUri(itemID);
			syncWarrantyItem(uri);
		}
	}

	private void syncWarrantyItem(Uri uri)
	{
		SyncAdapter.syncNow(this);
	}
}
