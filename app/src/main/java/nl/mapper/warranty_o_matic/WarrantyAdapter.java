package nl.mapper.warranty_o_matic;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import nl.mapper.warranty_o_matic.data.Util;
import nl.mapper.warranty_o_matic.data.WarrantyContract;
import nl.mapper.warranty_o_matic.view.helper.ImageLoader;
import nl.mapper.warranty_o_matic.view.holder.WarrantyHolder;


/**
 * Adapter to populate list/grid view with warranty cards.
 */
public class WarrantyAdapter extends CursorAdapter
{
	public WarrantyAdapter(Context context, Cursor c, int flags)
	{
		super(context, c, flags);
	}


	public WarrantyAdapter(Context context, Cursor c, boolean autoRequery)
	{
		super(context, c, autoRequery);
	}

	public static final String[] WARRANTY_PROJECTION ={
			WarrantyContract.WarrantyInfo._ID,
			WarrantyContract.WarrantyInfo.COLUMN_FRIENDLY_NAME,
			WarrantyContract.WarrantyInfo.COlUMN_MAKE,
			WarrantyContract.WarrantyInfo.COlUMN_MODEL,
			WarrantyContract.WarrantyInfo.COlUMN_WARRANTY_DATE,
			WarrantyContract.WarrantyInfo.COlUMN_PURCHASE_DATE,
			WarrantyContract.WarrantyInfo.COlUMN_STORE,
			WarrantyContract.WarrantyInfo.COlUMN_SERIAL,
			WarrantyContract.WarrantyInfo.COlUMN_IMAGE_URI,
			WarrantyContract.WarrantyInfo.COlUMN_RECEIPT_URI
	};
	public static final int COL_ID				= 0;
	public static final int COL_NAME 			= 1;
	public static final int COL_MAKE 			= 2;
	public static final int COL_MODEL 			= 3;
	public static final int COL_WARRANTY_DATE 	= 4;
	public static final int COL_PURCHASE_DATE 	= 5;
	public static final int COL_PURCHASE_STORE	= 6;
	public static final int COL_PURCHASE_SERIAL	= 7;
	public static final int COL_IMAGE 			= 8;
	public static final int COL_RECEIPT 		= 9;

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent)
	{
		int idToInflate = R.layout.warranty_card;
		View view = LayoutInflater.from(context).inflate(idToInflate, parent, false);
		WarrantyHolder holder = new WarrantyHolder(view);
		view.setTag(holder);
		return view;
	}
	private static Calendar getCalculationCalendar()
	{
		return new GregorianCalendar(TimeZone.getTimeZone("UTC"));
	}


	/**
	 * Format date
	 * @param date unix time (seconds since ...)
	 * @return date formatted as string
	 */
	public static String formatDate(long date)
	{
		Calendar cal = getCalculationCalendar();
		cal.setTimeInMillis(date*1000L);
		return formatDate(cal);
	}

	public static String formatDate(Calendar date)
	{
		DateFormat fmt = DateFormat.getDateInstance();
		fmt.setCalendar(date);
		return fmt.format(date.getTime());
	}

	public static Calendar getCurrentDate()
	{
		Calendar cal = getCalculationCalendar();
		cal.setTimeInMillis(timeToDate(System.currentTimeMillis()/1000));
		return cal;
	}

	public static long SECONDS_PER_DAY = 86400;

	public static long timeToDate(long unixTime)
	{
		return (unixTime / SECONDS_PER_DAY) * SECONDS_PER_DAY;
	}

	public static long calculateWarrantyDaysLeft(long warrantyDate)
	{
		long now 		= timeToDate(System.currentTimeMillis()/1000) / SECONDS_PER_DAY;
		warrantyDate 	= warrantyDate / SECONDS_PER_DAY;
		return warrantyDate - now;
	}
	@Override
	public void bindView(View view, Context context, Cursor cursor)
	{
		WarrantyHolder holder = (WarrantyHolder) view.getTag();

		holder.productName.setText(cursor.getString(COL_NAME));

		String makeModel = cursor.getString(COL_MAKE) + cursor.getString(COL_MODEL);
		holder.makeModel.setText(makeModel);

		String warrantyDate = formatDate(cursor.getLong(COL_WARRANTY_DATE));
		holder.warrantyDate.setText(warrantyDate);

		String purchaseDate = formatDate(cursor.getLong(COL_PURCHASE_DATE));
		holder.purchaseDate.setText(purchaseDate);

		String storeString = cursor.getString(COL_PURCHASE_STORE);
		holder.purchaseStore.setText(storeString);

		holder.serialNumber.setText(cursor.getString(COL_PURCHASE_SERIAL));

		long daysLeft = calculateWarrantyDaysLeft(cursor.getLong(COL_WARRANTY_DATE));
		holder.warrantyLeft.setText(Long.toString(daysLeft));
		String imageFileName = cursor.getString(COL_IMAGE);
		Uri productImageUri = null;
		if (imageFileName.length()>0)
			productImageUri = Util.imageNameToUri(context,imageFileName);

		int width = Math.round(context.getResources().getDimension(R.dimen.warranty_card_width));
		int height = Math.round(context.getResources().getDimension(R.dimen.warranty_card_heigth)/2);
		ImageLoader.loadImage(holder.productImage, productImageUri, context.getResources(), R.drawable.image, width, height);

		holder.receiptIcon.setOnClickListener(receiptClickListener);
		Uri receiptUri = Util.imageNameToUri(context, cursor.getString(COL_RECEIPT));
		holder.receiptIcon.setTag(receiptUri);

	}

	View.OnClickListener receiptClickListener = new View.OnClickListener(){
		@Override
		public void onClick(View v)
		{
			Uri receiptUri = (Uri)v.getTag();
			Intent imageViewIntent = new Intent(Intent.ACTION_VIEW);
			imageViewIntent.setDataAndType(receiptUri,"image/*");
			Context context = v.getContext();
			if(context != null && imageViewIntent.resolveActivity(context.getPackageManager()) != null)
				context.startActivity(imageViewIntent);
		}
	};
}
