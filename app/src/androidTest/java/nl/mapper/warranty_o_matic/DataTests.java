package nl.mapper.warranty_o_matic;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.ContactsContract;
import android.test.AndroidTestCase;

import org.json.JSONException;

import nl.mapper.warranty_o_matic.data.Util;
import nl.mapper.warranty_o_matic.data.WarrantyContract.WarrantyInfo;
import nl.mapper.warranty_o_matic.data.WarrantyDbHelper;

import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Tests database functionality
 */
public class DataTests extends AndroidTestCase
{
    private static final long valiDate = 1399379696000L;
    private static final String basePath = "file://sdcard/blaat/testing123/";
    private static final String IMAGE_FILENAME = "IMG_20140506_123456.jpg";
    public void testGetSyncID()
    {


        convertAndValidateUriDate(valiDate, Uri.parse(basePath + IMAGE_FILENAME));
        convertAndValidateUriDate(valiDate, Uri.parse(basePath + Long.toString(valiDate) + ".jpg"));
    }
    public void convertAndValidateUriDate(long valiDate, Uri uri)
    {
        long date = WarrantyInfo.getSyncIdFromImageUri(uri);
        assertEquals("Error parsing date", valiDate, date);
    }


    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        deleteDb();

        dbHelper = new WarrantyDbHelper(mContext, DBName, null, 1);
        db = dbHelper.getWritableDatabase();
    }

    private void insertTestData()
    {
        db.insert(WarrantyInfo.TABLE_NAME, null, getTestValues(1));
    }

    private void deleteDb()
    {
        if(dbHelper != null)
            dbHelper.close();
        getContext().deleteDatabase(DBName);
    }

    public void testDbUpgrade()
    {
        insertTestData();
        for (int i = 1; i <= WarrantyDbHelper.DATABASE_VERSION; i++)
        {
            dbHelper.close();
            dbHelper = new WarrantyDbHelper(mContext, DBName, null, i);
            db = dbHelper.getReadableDatabase();
            assertEquals("DB version mismatch", i, db.getVersion());
            Cursor result = db.rawQuery("SELECT * FROM "+WarrantyInfo.TABLE_NAME, null);
			ContentValues values = getTestValues(i);
			// Impossible to guess sync time here
			if(i == 2 && result.moveToFirst())
			{
				values.remove(WarrantyInfo.COLUMN_SYNC_TIME);
				values.put(WarrantyInfo.COLUMN_SYNC_TIME,result.getLong(result.getColumnIndex(WarrantyInfo.COLUMN_SYNC_TIME)));
			}
            assertEquals("Result doesn't match", values, result);
        }
    }

    static void assertEquals(String message, ContentValues values, Cursor cursor)
    {
        // Check if there is an actual result in the Cursor
        assertTrue(message + ": No Cursor Data", cursor.moveToFirst());
        assertEquals(message + ": Number of columns mismatch", values.size(), cursor.getColumnCount());
        Set<Map.Entry<String, Object>> valueSet = values.valueSet();
        for (Map.Entry<String, Object> entry : valueSet) {
            String columnName = entry.getKey();
            int index = cursor.getColumnIndex(columnName);
            assertFalse(message + ": Column not found: " + columnName, index == -1);
            String expectedValue = null;
            try
            {
                expectedValue = entry.getValue().toString();
            }
            catch (NullPointerException e)
            {}
            assertEquals(message,expectedValue, cursor.getString(index));
        }
    }

    static void assertEquals(String message, ContentValues left, ContentValues right)
    {
        assertEquals(message + ": Number of entries mismatch", left.size(), right.size());
        Set<Map.Entry<String, Object>> valueSet = left.valueSet();
        for (Map.Entry<String, Object> entry : valueSet) {
            String columnName = entry.getKey();
            String rightValue = right.getAsString(columnName);
            String leftValue = null;
            try
            {
                leftValue = entry.getValue().toString();
            }
            catch (NullPointerException e)
            {}
            assertEquals(message, leftValue, rightValue);
        }
    }
    static void assertEquals(ContentValues left, ContentValues right)
    {
       assertEquals("", left,right);
    }


    private SQLiteDatabase db;
    private File dbFile;
    private File dbDir;
    private WarrantyDbHelper dbHelper;

    private static final String DBName = "testDB.sqlite";

    // DB version 1 test values
    private static final long 	TEST_VALUE_ID               = 1;
    private static final String TEST_VALUE_FRIENDLY_NAME    = "Test record";
    private static final long   TEST_VALUE_PURCHASE_DATE    = valiDate/1000;
    private static final long   TEST_VALUE_WARRANTY_DATE    = TEST_VALUE_PURCHASE_DATE + TimeUnit.DAYS.toSeconds(365);
    private static final String TEST_VALUE_STORE            = "Store of everything";
    private static final String TEST_VALUE_SERIAL           = "12345abcde";
    private static final String TEST_VALUE_MAKE             = "Some Company";
    private static final String TEST_VALUE_MODEL            = "Awesometron 5000 Pro";
    private static final String TEST_VALUE_IMAGE_URI        = basePath + IMAGE_FILENAME;
    private static final String TEST_VALUE_RECEIPT_URI      = TEST_VALUE_IMAGE_URI;

    // DB version 2 test values
    private static final long TEST_VALUE_SYNC_ID          = valiDate;
    // No time is inserted upon upgrade
    private static final long TEST_VALUE_SYNC_TIME        = 0;

    /**
     * Creates test values for a specific version of the database. These can be used as a base of test data, or to verify an upgraded database.
     * @param version version of the database to generate the values for
     * @return test values
     */
    ContentValues getTestValues(int version)
    {
        ContentValues out = new ContentValues();



                out.put(WarrantyInfo._ID, TEST_VALUE_ID );
                out.put(WarrantyInfo.COLUMN_FRIENDLY_NAME, TEST_VALUE_FRIENDLY_NAME);
                out.put(WarrantyInfo.COlUMN_PURCHASE_DATE, TEST_VALUE_PURCHASE_DATE);
                out.put(WarrantyInfo.COlUMN_WARRANTY_DATE, TEST_VALUE_WARRANTY_DATE);
                out.put(WarrantyInfo.COlUMN_STORE, TEST_VALUE_STORE);
                out.put(WarrantyInfo.COlUMN_SERIAL, TEST_VALUE_SERIAL);
                out.put(WarrantyInfo.COlUMN_MAKE, TEST_VALUE_MAKE);
                out.put(WarrantyInfo.COlUMN_MODEL, TEST_VALUE_MODEL);
        if(version == 1)
        {
            out.put(WarrantyInfo.COlUMN_IMAGE_URI, TEST_VALUE_IMAGE_URI);
            out.put(WarrantyInfo.COlUMN_RECEIPT_URI, TEST_VALUE_RECEIPT_URI);
        }
        else
        {
            out.put(WarrantyInfo.COlUMN_IMAGE_URI, IMAGE_FILENAME);
            out.put(WarrantyInfo.COlUMN_RECEIPT_URI, IMAGE_FILENAME);
        }


        if (version > 1)
        {
            out.put(WarrantyInfo.COLUMN_SYNC_ID, TEST_VALUE_SYNC_ID);
            out.put(WarrantyInfo.COLUMN_SYNC_TIME, TEST_VALUE_SYNC_TIME);
        }
        return out;
    }

	public static final String JSON_VERIFY_STRING = "{\"model\":\"Awesometron 5000 Pro\",\"_id\":1,\"store\":\"Store of everything\",\"syncTime\":0,\"name\":\"Test record\",\"image\":\"IMG_20140506_123456.jpg\",\"receipt\":\"IMG_20140506_123456.jpg\",\"warranty\":1430915696,\"date\":1399379696,\"serial\":\"12345abcde\",\"make\":\"Some Company\",\"syncID\":1399379696000}";

	public void testConvertToJSON()
    {
        String json = Util.convertRecordToJson(getTestValues(WarrantyDbHelper.DATABASE_VERSION));
        assertEquals("Json conversion failed", JSON_VERIFY_STRING,json);
    }
	public void testConvertFromJSON() throws JSONException
	{
		ContentValues values = Util.convertJsonToRecord(JSON_VERIFY_STRING);
		assertEquals(values, getTestValues(WarrantyDbHelper.DATABASE_VERSION));

	}

}
